# [About HammerThemes](https://www.youtube.com/watch?v=KNoKp7TTf0Y)
HammerThemes is a simple tool that allows you to easily create your own color themes for Valve's Hammer Editor.

* **Customize** most colors of the 2d viewports, including the background color, grid and most tools.
* **Share** your custom themes with others by exporting them to `.het` files, which can be imported back at any point.
* Easily see what you are doing using the **preview window** on the right half of the program.
* Automatically **manages all installations** of Hammer Editor for you - no need to specify any installation paths or the like.
* Planned features:
	* Further customization possibilities (such as selections in the 3d viewport, entity icons, tool icons).
	* More advanced preview window (including preview of the different tools, for example).

# Download

**[Download the latest version of HammerThemes here!](https://gitlab.com/redmser/HammerThemes/tags/)**

# Screenshots

![](https://i.imgur.com/4EkemaE.png)

![](https://i.imgur.com/ixo3Vnv.png)

# Installation
After [downloading the latest release](https://gitlab.com/redmser/HammerThemes/tags/), simply extract the folder's contents to a location of your choice and run the `HammerThemes.exe` file.

You will need the [.NET Framework 4.5.2](https://www.microsoft.com/en-us/download/details.aspx?id=42642) redistributables in order for the program to launch.

# Troubleshooting
* Color changes inside of Hammer will only show after restarting the editor (if it was running when the theme was applied).
* Certain colors can not be changed. Those include:
	* Default colors of brushes (unless they are in a visgroup)
	* Colors of point and brush entities
	* Cordons and radius culling
	* Entity creation tool
	* Vertex editing (3d view)
* In order to correctly see custom background colors show up, the white-on-black setting inside of Hammer Editor has to be enabled. Otherwise, certain custom colors will not show up.
* If you want certain parts of the grid lines to be affected by the grid intensity slider in Hammer Editor's settings, simply tick the `Intensity?` checkbox of the corresponding grid color inside of HammerThemes.

# Contributing
Since this is a pretty small project, feature requests and bug reports on the **[issues section](https://gitlab.com/redmser/HammerThemes/issues)** will mostly be managed by me.

Same would go for any custom color themes that you would like to be added to the official release of HammerThemes (either self-made, or based off of other themes) - just let them get to me in some way! :)

If you still wish to add other kinds of functionality yourself, go ahead and do whatever on a **fork**, I'll be sure to check out what you're up to!

You can also help me a lot by **donating**, since it motivates me to continue on other projects! Check the donate options inside of HammerThemes for more info.

# Credits
* RedMser for main coding and UI
* 9joao6 for testing
* [Newtonsoft.Json](https://www.newtonsoft.com/json) library
* [ObjectListView](http://objectlistview.sourceforge.net/cs/index.html) library