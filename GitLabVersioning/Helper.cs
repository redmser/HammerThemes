﻿using System;
using System.ComponentModel;
using System.Net;

namespace GitlabVersioning
{
    /// <summary>
    /// Collection of helper methods.
    /// </summary>
    internal static class Helper
    {
        /// <summary>
        /// Gets the response of a blocking HTTP call.
        /// </summary>
        /// <param name="url">The URL to connect to.</param>
        /// <param name="timeout">The timeout in milliseconds.</param>
        /// <param name="token">The PRIVATE-TOKEN header value.</param>
        /// <param name="proxy">The proxy to use for the HTTP connection.</param>
        internal static HttpWebResponse GetResponse(string url, int timeout, IWebProxy proxy)
        {
            var req = PrepareRequest(url, timeout, proxy);
            return req.GetResponse() as HttpWebResponse;
        }

        private static HttpWebRequest PrepareRequest(string url, int timeout, IWebProxy proxy)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            var req = WebRequest.CreateHttp(url);
            req.Timeout = timeout;
            req.UserAgent = "HammerThemes API, by RedMser"; 
            req.Proxy = proxy;
            return req;
        }

        /// <summary>
        /// Returns the DescriptionAttribute string of the specified enum value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static string GetEnumDescription(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }
}
