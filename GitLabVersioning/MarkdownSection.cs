﻿using System;

namespace GitlabVersioning
{
    /// <summary>
    /// A section in a markdown document. Defined as a headline followed by text paragraphs.
    /// </summary>
    [Serializable]
    public class MarkdownSection
    {
        /// <summary>
        /// Title of the section.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Contents of the section, including regular markdown formatting.
        /// </summary>
        public string Contents { get; set; }
    }
}
