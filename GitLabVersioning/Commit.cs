﻿using System;
using Newtonsoft.Json.Linq;

namespace GitlabVersioning
{
    /// <summary>
    /// Represents a commit and all information regarding it.
    /// </summary>
    public class Commit
    {
        /// <summary>
        /// Gets the message that the tag was committed with.
        /// </summary>
        public string TagMessage { get; private set; }

        /// <summary>
        /// Gets the message of the commit that this tag was assigned to.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the commit id.
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// Gets the parent commit ids.
        /// </summary>
        public string[] ParentIds { get; private set; }

        /// <summary>
        /// Gets the commit authored date.
        /// </summary>
        public DateTime AuthoredDate { get; private set; }

        /// <summary>
        /// Gets the author name of the commit.
        /// </summary>
        public string AuthorName { get; private set; }

        /// <summary>
        /// Gets the committer name of the commit.
        /// </summary>
        public string CommitterName { get; private set; }

        /// <summary>
        /// Gets the author email of the commit.
        /// </summary>
        public string AuthorEmail { get; private set; }

        /// <summary>
        /// Gets the committer email of the commit.
        /// </summary>
        public string CommitterEmail { get; private set; }

        /// <summary>
        /// Gets the committed date of the commit.
        /// </summary>
        public DateTime CommittedDate { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{this.TagMessage} by {this.CommitterName} @ {this.CommittedDate}";

        /// <summary>
        /// Generates a new Commit from the specified token containing infos about this commit.
        /// </summary>
        /// <param name="tagmsg">The message of the tag, since this is included in the tag's properties but makes more sense to be in the commit information.</param>
        /// <param name="ctok">The token containing the infos.</param>
        /// <returns></returns>
        internal static Commit FromToken(string tagmsg, JToken ctok)
        {
            var commit = new Commit();
            commit.Id = ctok.Value<string>("id");
            commit.Message = ctok.Value<string>("message");
            commit.TagMessage = tagmsg;
            commit.ParentIds = ctok["parent_ids"].ToObject<string[]>();
            commit.AuthoredDate = ctok.Value<DateTime>("authored_date");
            commit.AuthorName = ctok.Value<string>("author_name");
            commit.AuthorEmail = ctok.Value<string>("author_email");
            commit.CommittedDate = ctok.Value<DateTime>("committed_date");
            commit.CommitterName = ctok.Value<string>("committer_name");
            commit.CommitterEmail = ctok.Value<string>("committer_email");
            return commit;
        }
    }
}
