﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GitlabVersioning
{
    /// <summary>
    /// A tag of the project.
    /// </summary>
    [JsonConverter(typeof(TagConverter))]
    [Serializable]
    public class Tag
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tag" /> class.
        /// </summary>
        public Tag()
        {

        }

        /// <summary>
        /// Generates a new Tag from the specified token containing infos about this tag.
        /// </summary>
        /// <param name="token">The token containing the infos.</param>
        /// <returns></returns>
        internal static Tag FromToken(JToken token)
        {
            var ctok = token["commit"];
            var rtok = token["release"];
            var tag = new Tag();
            //TODO: Tag - Why is tag name included twice in the returned JSON ("name" and "release.tag_name")? when are they ever different?
            tag.TagName = token.Value<string>("name");
            tag.ReleaseNotes = rtok.Value<string>("description");
            tag.Commit = Commit.FromToken(token.Value<string>("message"), ctok);

            var reader = new MarkdownReader(tag.ReleaseNotes);
            reader.Parse();
            tag.ReleaseNotesSections = reader.Sections;
            tag.Title = tag.ReleaseNotesSections[0].Title;
            tag.Description = tag.ReleaseNotesSections[0].Contents;

            //Releases and changes need to be done afterwards!
            return tag;
        }

        /// <summary>
        /// Gets the release notes of the tag with their original markdown formatting.
        /// <para/>Only use this if you wish to reformat the contents yourself, or if you wish to display the original release notes.
        /// </summary>
        public string ReleaseNotes { get; private set; }

        /// <summary>
        /// Gets the release notes of the tag split into the different headlines.
        /// <para/>Only use this if you wish to reformat the contents yourself, or if you wish to display the original release notes.
        /// </summary>
        public MarkdownSection[] ReleaseNotesSections { get; private set; }

        /// <summary>
        /// Gets the commit information that this tag was commited with.
        /// </summary>
        public Commit Commit { get; private set; }

        /// <summary>
        /// Gets the name of the tag.
        /// </summary>
        public string TagName { get; private set; }

        /// <summary>
        /// Gets the title of the release notes. This is the first line of the release notes without formatting.
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Gets a summarized description of this tag. This is whatever is between the title and the next title-formatted line of text, using markdown.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the list of changes. Uses list formatting which can be configured from the TagList, remaining markdown is included.
        /// </summary>
        public Change[] Changes { get; internal set; }

        /// <summary>
        /// Gets the list of releases attached to this tag.
        /// </summary>
        public Release[] Releases { get; internal set; }
    }
}
