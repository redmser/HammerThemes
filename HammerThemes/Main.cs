﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using HammerThemes.Properties;
using System.Threading;

namespace HammerThemes
{
    /// <summary>
    /// Main dialog of this application.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Main : Form
    {
        //NYI: Main - allow theming 3d selection using custom VMT (see hl2/materials/editor for vtf and vmt) + generated (or user-selected) VTF file for plain color or custom select texture
        //  -> expand to other areas - check materials/editor folder of HL2 for some ideas, like theming point entity models/entity icons/other hammer icons/etc
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Main"/> class.
        /// </summary>
        public Main()
        {
            //Compiler-generated
            InitializeComponent();

            //Upgrade settings if first startup (since new settings are generated for the new program instance)
            if (Settings.Default.FirstStartup)
            {
                try
                {
                    //This may throw a NullReferenceException if no previous version exists
                    var wasfirst = (bool)Settings.Default.GetPreviousVersion("FirstStartup");

                    Settings.Default.Upgrade();
                    Settings.Default.Save();

                    //Update whether it really was first startup
                    Settings.Default.FirstStartup = wasfirst;
                }
                catch (NullReferenceException)
                {
                    //Do not do anything else
                }
            }

            //OLV
            ObjectListView.EditorRegistry.Register(typeof(Color), (o, c, v) => new ColorEditor(o));

            //Add theme list
            Theme.PopulateDefaultColors();
            if (Settings.Default.Themes == null)
            {
                //Default theme list
                Settings.Default.Themes = new List<Theme> { new DefaultTheme() };
            }
            else
            {
                //Load properties of colors
                foreach (var theme in Settings.Default.Themes)
                {
                    if (theme is DefaultTheme)
                        continue;

                    theme.NameChanged += (s, e) => this.UpdateThemeCombobox();

                    //Copy non-serialized properties
                    foreach (var tc in theme.Colors)
                    {
                        var corr = Theme.DefaultColors.FirstOrDefault(c => c.Name == tc.Name);
                        if (corr == null)
                            continue;
                        tc.Description = corr.Description;
                        tc.Text = corr.Text;
                        tc.Unused = corr.Unused;
                        tc.CanScale = corr.CanScale;
                    }
                }
            }

            //Theme selection
            Theme currtheme = Settings.Default.Themes.FirstOrDefault(t => t.Name == Theme.CurrentTheme);

            //Fallback selection
            if (currtheme == null)
                currtheme = Settings.Default.Themes.FirstOrDefault(c => c is DefaultTheme);
            this.UpdateSelectedTheme(currtheme);
            this.themeLabel.Text = "Current Theme: " + currtheme.Name;

            //Update stuff
            this.UpdateFilter(this.unusedCheckBox.Checked);

            //Add version number to title
            this.Text = "HammerThemes v" + Updates.CurrentVersion;

            //Enable sorting last
            this.colorsObjectListView.Sort(this.colorTextColumn, SortOrder.Ascending);
            //FIXME: Main - disable rendering of checkboxes when CanScale is false
            this.colorIntensityColumn.AspectPutter = (c, v) => { if (((ThemeColor)c).CanScale) ((ThemeColor)c).Scales = (bool)v; };
            this.colorIntensityColumn.AspectToStringConverter = (v) => "";
        }

        private void Main_Load(object sender, EventArgs e)
        {
            if (!Settings.Default.FirstStartup)
            {
                //Got settings to reload
                this.Size = Settings.Default.WindowSize;
                this.Location = Settings.Default.WindowLocation;
                this.WindowState = Settings.Default.WindowState;
                try
                {
                    this.colorsObjectListView.RestoreState(Settings.Default.OLVState);
                }
                catch
                {
                }
            }
            else
            {
                //Init values
                Settings.Default.PreviewBrush = new GoodRectangle(this.previewPanel.Width / 2 + 30, this.previewPanel.Height / 2 + 20, 70, 90);

                //No longer the first startup
                Settings.Default.FirstStartup = false;
            }

            //Initialize preview
            this.previewPanel.DottedGrid = this.dottedGridCheckBox.Checked;
            this.previewPanel.GridIntensity = (byte)this.gridIntensityTrackBar.Value;
            this.previewPanel.GridSize = this.gridSizeTrackBar.Value;
            this.previewPanel.PreviewBrush = Settings.Default.PreviewBrush;
            this.previewPanel.Show();

            if (Settings.Default.UpdateFrequency != 0)
            {
                this.CheckUpdatesStartup();
            }
        }

        private void CheckUpdatesStartup()
        {
            //Update check settings
            TimeSpan timediff;
            switch (Settings.Default.UpdateFrequency)
            {
                case 1:
                    //On launch - check!
                    DoCheck();
                    return;
                case 2:
                    //Daily
                    timediff = new TimeSpan(1, 0, 0, 0);
                    break;
                case 3:
                    //Weekly
                    timediff = new TimeSpan(7, 0, 0, 0);
                    break;
                case 4:
                    //Monthly
                    timediff = new TimeSpan(30, 0, 0, 0);
                    break;
                default:
                    throw new ArgumentException();
            }

            //Check if last update was that long ago
            var last = Settings.Default.UpdateLastCheck;
            if (last != DateTime.MinValue && DateTime.Now - last >= timediff)
            {
                //Do update check!
                DoCheck();
            }
            else
            {
                //See if we're to be reminded
                var remind = Settings.Default.UpdateRemindDate;
                if (remind != DateTime.MinValue && remind > DateTime.Now)
                {
                    //Reset our reminding date
                    Settings.Default.UpdateRemindDate = DateTime.MinValue;

                    //Do update check!
                    DoCheck();
                }
            }



            async void DoCheck()
            {
                var success = await Updates.CheckUpdateAsync();
                if (success)
                {
                    if (Updates.IsOutdated)
                    {
                        //Check settings on how to respond to this
                        switch (Settings.Default.UpdateAvailable)
                        {
                            case 0:
                                //Just tell me
                                Updates.ShowUpdateAvailable(false);
                                break;
                            case 1:
                                //Download and then tell me
                                var path = await Updates.DownloadUpdateAsync(new CancellationTokenSource().Token, new Progress<int>());
                                Updates.ShowUpdateAvailable(false, path);
                                break;
                        }
                    }
                }
                else
                {
                    //ERROR!
                    Updates.ShowUpdateError();
                }
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Apply current theme
            if (this._applyThemeOnClose)
                this.SelectedTheme?.Apply();
            else if (this.SelectedTheme != null && Theme.CurrentTheme != this.SelectedTheme.Name)
            {
                //Current theme is different, ask user whether to apply selected
                var isolddefault = Theme.CurrentTheme == "(Default Theme)";
                var isnewdefault = this.SelectedTheme is DefaultTheme;
                var oldtheme = isolddefault ? "default" : $"\"{Theme.CurrentTheme}\"";
                var newtheme = isnewdefault ? "the default theme" : $"\"{this.SelectedTheme.Name}\"";
                var res = MessageBox.Show($"Hammer Editor is currently using the {oldtheme} theme.\n\nWould you like to apply {newtheme} instead?", "Apply theme?",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (res == DialogResult.Yes)
                {
                    //Apply the theme!
                    this.SelectedTheme.Apply();
                }
                else if (res == DialogResult.Cancel)
                {
                    //Wait up, don't close!
                    e.Cancel = true;
                    return;
                }
            }

            //Store state
            Settings.Default.WindowState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
            {
                Settings.Default.WindowSize = this.Size;
                Settings.Default.WindowLocation = this.Location;
            }
            else
            {
                Settings.Default.WindowSize = this.RestoreBounds.Size;
                Settings.Default.WindowLocation = this.RestoreBounds.Location;
            }
            Settings.Default.OLVState = this.colorsObjectListView.SaveState();
            Settings.Default.PreviewBrush = this.previewPanel.PreviewBrush;

            //Save settings
            Settings.Default.Save();
        }

        private bool _applyThemeOnClose;

        private void newButton_Click(object sender, System.EventArgs e)
        {
            //Add new theme to list
            var theme = new Theme();
            theme.Name = Theme.NewThemeName(Settings.Default.Themes.Select(t => t.Name));
            theme.InitializeColors();
            theme.NameChanged += (s, a) => this.UpdateThemeCombobox();
            this.UpdateSelectedTheme(theme);
        }

        private void cloneButton_Click(object sender, System.EventArgs e)
        {
            //Create new theme, copying the color values
            if (this.SelectedTheme is DefaultTheme)
            {
                //Same as pressing new
                this.newButton_Click(null, EventArgs.Empty);
            }
            else
            {
                //Copy color values over
                var theme = new Theme();
                theme.Name = $"{Theme.NewThemeName(Settings.Default.Themes.Select(t => t.Name))} ({this.SelectedTheme.Name})";
                theme.InitializeColors();
                foreach (var tc in theme.Colors)
                {
                    var corr = this.SelectedTheme.Colors.FirstOrDefault(c => c.Name == tc.Name);
                    tc.Enabled = corr.Enabled;
                    tc.Color = corr.Color;
                }
                theme.NameChanged += (s, a) => this.UpdateThemeCombobox();
                this.UpdateSelectedTheme(theme);
            }
        }

        private void deleteButton_Click(object sender, System.EventArgs e)
        {
            //Delete selected theme (when confirmed)
            var res = MessageBox.Show($"Would you like to delete the selected theme \"{this.SelectedTheme}\"?", "Delete selected theme", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (res == DialogResult.Yes)
            {
                //Delete!!!
                Settings.Default.Themes.Remove(this.SelectedTheme);
                this.UpdateSelectedTheme(Settings.Default.Themes.Last());
                this.UpdateThemeCombobox();
            }
        }

        private void applyButton_Click(object sender, System.EventArgs e)
        {
            //Apply current theme
            this.SelectedTheme?.Apply();
            this.themeLabel.Text = "Current Theme: " + this.SelectedTheme.Name;
            this._applyThemeOnClose = false;

            //Save settings
            Settings.Default.Save();
        }

        /// <summary>
        /// Gets the selected theme instance.
        /// </summary>
        public Theme SelectedTheme => this.themesComboBox.SelectedItem as Theme;

        /// <summary>
        /// Selects the specified theme and updates the UI accordingly.
        /// </summary>
        /// <param name="theme">The theme to select.</param>
        public void UpdateSelectedTheme(Theme theme)
        {
            if (theme == null)
                throw new ArgumentNullException(nameof(theme));

            if (!Settings.Default.Themes.Contains(theme))
                Settings.Default.Themes.Add(theme);
            this.UpdateThemeCombobox();
            this.themesComboBox.SelectedItem = theme;
            this.UpdateSelectedTheme();
        }

        /// <summary>
        /// Updates the combobox containing the list of themes.
        /// </summary>
        public void UpdateThemeCombobox()
        {
            var selname = this.SelectedTheme?.Name;
            this.themesComboBox.Items.Clear();
            foreach (var t in Settings.Default.Themes.OrderBy(t => t.Name))
            {
                this.themesComboBox.Items.Add(t);
            }
            this.themesComboBox.SelectedItem = this.themesComboBox.Items.OfType<Theme>().FirstOrDefault(t => t.Name == selname) ?? this.themesComboBox.Items[0];
            this.themesComboBox.DisplayMember = "Name";
            this.themesComboBox.ValueMember = null;
        }

        /// <summary>
        /// Updates the UI to reflect the selected theme instance.
        /// </summary>
        public void UpdateSelectedTheme()
        {
            this.nameTextBox.DataBindings.Clear();
            this.previewPanel.Theme = this.SelectedTheme;
            this.previewPanel.Invalidate();

            if (this.SelectedTheme == null || this.SelectedTheme is DefaultTheme)
            {
                //Disable controls
                this.nameTextBox.Text = "";
                this.colorsObjectListView.ClearObjects();
                this.groupBox.Enabled = false;
                this.deleteButton.Enabled = false;
                this.exportButton.Enabled = false;
                return;
            }

            this.deleteButton.Enabled = true;
            this.exportButton.Enabled = true;
            this.groupBox.Enabled = true;
            this.nameTextBox.DataBindings.Add("Text", this.SelectedTheme, "Name", false, DataSourceUpdateMode.OnPropertyChanged);
            this.nameTextBox.Text = this.SelectedTheme.Name;
            this.colorsObjectListView.SetObjects(this.SelectedTheme.Colors);
        }

        private void themesComboBox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            this.UpdateSelectedTheme();
            this._applyThemeOnClose = false;
        }

        private void colorsObjectListView_FormatCell(object sender, FormatCellEventArgs e)
        {
            if (e.Column == this.colorColorColumn)
            {
                //Show color in its column
                e.SubItem.Text = "";
                e.SubItem.BackColor = (Color)e.CellValue;
            }
            else
            {
                //Background color depending on enabled state
                e.SubItem.BackColor = ((ThemeColor)e.Model).Enabled ? SystemColors.Control : SystemColors.ControlDark;
            }
        }

        private void colorsObjectListView_CellEditStarting(object sender, CellEditEventArgs e)
        {
            e.Control.Location = e.CellBounds.Location;
            e.Control.Size = e.CellBounds.Size;
            this.ShowHelp((ThemeColor)e.RowObject);

            if (e.Control is TextBox tb)
            {
                //Disable autocomplete
                tb.AutoCompleteMode = AutoCompleteMode.None;
            }
        }

        private void colorsObjectListView_CellEditValidating(object sender, CellEditEventArgs e)
        {
            if (e.Column == this.colorValueColumn)
            {
                //Try applying new color
                var tester = new ThemeColor();
                try
                {
                    tester.ColorString = e.NewValue.ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not save the specified value as a Color.\n\nExpected format: R G B (for example: 128 0 255)",
                        "Error saving value", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    return;
                }
            }

            //Changed value!
            var valuechanged = false;
            if (e.Value is Color c && !c.TrulyEquals((Color)e.NewValue))
                valuechanged = true;
            if (e.Value is string s && !s.Equals(e.NewValue))
                valuechanged = true;
            if (valuechanged && (e.Column == this.colorValueColumn || e.Column == this.colorColorColumn))
            {
                ((ThemeColor)e.RowObject).Enabled = true;
                this._applyThemeOnClose = true;
            }

            //Update preview
            this.previewPanel.Invalidate();
        }

        private void colorsObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            this.ShowHelp(this.colorsObjectListView.SelectedObject as ThemeColor);
        }

        private void ShowHelp(ThemeColor tc)
        {
            if (tc == null)
            {
                //None or many
                if (this.colorsObjectListView.SelectedObjects.Count == 0)
                {
                    this.titleLabel.Text = "No color selected";
                    this.helpLabel.Text = "Select a color to get help for it.";
                }
                else
                {
                    this.titleLabel.Text = "Multiple colors selected";
                    this.helpLabel.Text = "Select a single color to get help for it.";
                }
            }
            else
            {
                this.titleLabel.Text = tc.Text == tc.Name ? tc.Text : $"{tc.Text} ({tc.Name})";
                this.helpLabel.Text = tc.Description;
            }
        }

        private void importButton_Click(object sender, EventArgs e)
        {
            //Select path
            var ofd = new OpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Filter = "Hammer Editor Themes (*.het)|*.het";
            ofd.Multiselect = true;
            ofd.Title = "Import theme files";

            var res = ofd.ShowDialog();
            if (res == DialogResult.OK)
            {
                //Load themes from files
                foreach (var file in ofd.FileNames)
                {
                    var theme = Theme.LoadTheme(file);
                    Settings.Default.Themes.Add(theme);
                    theme.NameChanged += (s, a) => this.UpdateThemeCombobox();
                }

                //Update & select
                this.UpdateThemeCombobox();
                this.themesComboBox.SelectedItem = Settings.Default.Themes.Last();
                this.UpdateSelectedTheme();
            }
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            //Select path
            var sfd = new SaveFileDialog();
            sfd.FileName = this.SelectedTheme.Name + ".het";
            sfd.AddExtension = true;
            sfd.CheckPathExists = true;
            sfd.DefaultExt = ".het";
            sfd.Filter = "Hammer Editor Themes (*.het)|*.het";
            sfd.OverwritePrompt = true;
            sfd.Title = "Export theme " + this.SelectedTheme.Name;

            var res = sfd.ShowDialog();
            if (res == DialogResult.OK)
            {
                //Save theme to file
                this.SelectedTheme.SaveTheme(sfd.FileName);
            }
        }

        private void donateButton_Click(object sender, EventArgs e)
        {
            //Show donate dialog
            new Donate().Show(this);
        }

        /// <summary>
        /// Updates the show unused filtering on the list.
        /// </summary>
        /// <param name="showUnused"></param>
        public void UpdateFilter(bool showUnused)
        {
            if (showUnused)
            {
                this.colorsObjectListView.AdditionalFilter = null;
            }
            else
            {
                this.colorsObjectListView.AdditionalFilter = new ThemeColorUnusedFilter();
            }
        }

        private void unusedCheckBox_CheckedChanged(object sender, EventArgs e) => this.UpdateFilter(this.unusedCheckBox.Checked);

        private void gridSizeTrackBar_Scroll(object sender, EventArgs e)
        {
            this.previewPanel.GridSize = this.gridSizeTrackBar.Value;
            this.previewPanel.Invalidate();
        }

        private void gridIntensityTrackBar_Scroll(object sender, EventArgs e)
        {
            this.previewPanel.GridIntensity = (byte)this.gridIntensityTrackBar.Value;
            this.previewPanel.Invalidate();
        }

        private void dottedGridCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.previewPanel.DottedGrid = this.dottedGridCheckBox.Checked;
            this.previewPanel.Invalidate();
        }

        private void colorsObjectListView_SubItemChecking(object sender, SubItemCheckingEventArgs e)
        {
            this.previewPanel.Invalidate();
        }

        private void editSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Open color picker
            using (var picker = new ColorEditor().colorDialog)
            {
                if (picker.ShowDialog() != DialogResult.OK)
                    return;

                //Apply selected color
                foreach (var tc in this.colorsObjectListView.SelectedObjects.Cast<ThemeColor>())
                {
                    //Store old color and update
                    var oldcolor = tc.Color;
                    tc.Color = picker.Color;

                    //Changed value?
                    if (!tc.Color.TrulyEquals(oldcolor))
                    {
                        this._applyThemeOnClose = true;
                        tc.Enabled = true;
                    }
                }
            }

            //Update preview and list display
            this.previewPanel.Invalidate();
            this.colorsObjectListView.RefreshSelectedObjects();
        }

        private void resetSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Set selected colors to their defaults
            foreach (var tc in this.colorsObjectListView.SelectedObjects.Cast<ThemeColor>())
            {
                //Store old color and update
                var oldcolor = tc.Color;
                tc.Color = Theme.DefaultColors.FirstOrDefault(t => t.Name == tc.Name).Color;
                tc.Enabled = false;

                //Changed value?
                if (!tc.Color.TrulyEquals(oldcolor))
                    this._applyThemeOnClose = true;
            }

            //Update preview and list display
            this.previewPanel.Invalidate();
            this.colorsObjectListView.RefreshSelectedObjects();
        }

        private void colorsContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var hasSelection = this.colorsObjectListView.SelectedObjects.Count > 0;
            this.resetSelectedToolStripMenuItem.Enabled = hasSelection;
            this.editSelectedToolStripMenuItem.Enabled = hasSelection;
        }

        private void colorsObjectListView_DoubleClick(object sender, EventArgs e)
        {
            if (this.colorsObjectListView.SelectedObjects.Count > 0)
                this.editSelectedToolStripMenuItem_Click(sender, e);
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            //Open the settings dialog
            new Preferences().Show(this);
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            //Show about screen
            new About(this.previewPanel).Show(this);
        }

        private void Main_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            //Always show about screen instead
            this.aboutButton_Click(sender, hlpevent);
        }
    }
}
