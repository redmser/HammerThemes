﻿using System;
using System.Drawing;

namespace HammerThemes
{
    /// <summary>
    /// A rectangle struct which includes additional properties for locations and sizes.
    /// </summary>
    [Serializable]
    public struct GoodRectangle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GoodRectangle"/> struct.
        /// </summary>
        /// <param name="rect">The source rectangle.</param>
        public GoodRectangle(Rectangle rect)
        {
            Left = rect.Left;
            Top = rect.Top;
            Width = rect.Width;
            Height = rect.Height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GoodRectangle"/> struct.
        /// </summary>
        public GoodRectangle(Point location, Size size)
        {
            Left = location.X;
            Top = location.Y;
            Width = size.Width;
            Height = size.Height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GoodRectangle"/> struct.
        /// </summary>
        public GoodRectangle(int left, int top, int width, int height)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Ensures that the rectangle stays within the specified rectangle.
        /// </summary>
        /// <returns></returns>
        public GoodRectangle Clamp(Rectangle bounds) =>
            new GoodRectangle(Math.Min(Math.Max(this.Left, bounds.Left), bounds.Right - this.Width),
                              Math.Min(Math.Max(this.Top, bounds.Top), bounds.Bottom - this.Height), this.Width, this.Height);

#pragma warning disable CS1591

        public static implicit operator Rectangle(GoodRectangle rect) => new Rectangle(rect.Left, rect.Top, rect.Width, rect.Height);

        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Bottom => Top + Height;
        public int Right => Left + Width;
        public int HalfWidth => Width / 2;
        public int HalfHeight => Height / 2;
        public int CenterX => Left + HalfWidth;
        public int CenterY => Top + HalfHeight;

        public Point TopLeft => new Point(Left, Top);
        public Point TopCenter => new Point(CenterX, Top);
        public Point TopRight => new Point(Right, Top);
        public Point BottomLeft => new Point(Left, Bottom);
        public Point BottomCenter => new Point(CenterX, Bottom);
        public Point BottomRight => new Point(Right, Bottom);
        public Point LeftCenter => new Point(Left, CenterY);
        public Point Center => new Point(CenterX, CenterY);
        public Point RightCenter => new Point(Right, CenterY);

        public bool Contains(Point point) => point.X >= Left && point.X <= Right && point.Y >= Top && point.Y <= Bottom;

#pragma warning restore CS1591

    }
}
