﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// Extension methods.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Returns this color with a different alpha value, if it can scale from intensity.
        /// </summary>
        /// <param name="tc">Color to modify.</param>
        /// <param name="alpha">New alpha value.</param>
        /// <returns></returns>
        public static Color ApplyAlpha(this ThemeColor tc, byte alpha)
        {
            if (tc.Scales)
                return tc.Color.ApplyAlpha(alpha);
            return tc.Color;
        }

        /// <summary>
        /// Returns this color with a different alpha value.
        /// </summary>
        /// <param name="color">Color to modify.</param>
        /// <param name="alpha">New alpha value.</param>
        /// <returns></returns>
        public static Color ApplyAlpha(this Color color, byte alpha) => Color.FromArgb(alpha, color.R, color.G, color.B);

        /// <summary>
        /// Whether two colors are truly equal (ignoring named colors junk).
        /// </summary>
        public static bool TrulyEquals(this Color c1, Color c2) => c1.ToArgb().Equals(c2.ToArgb());

        /// <summary>
        /// Ensures that an Invoke call doesn't crash stupidly.
        /// </summary>
        public static void TryInvoke(this Control control, Action action)
        {
            try
            {
                if (control == null || control.IsDisposed || !control.IsHandleCreated)
                    return;
                if (control.InvokeRequired)
                    control.Invoke(action);
                else
                    action();
            }
            catch
            {
                
            }
        }
    }
}
