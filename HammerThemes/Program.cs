﻿using System;
using System.Windows.Forms;

namespace HammerThemes
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //NYI: General - Add some good "default" themes, possibly from places like text editor syntax highlight schemes?
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }
    }
}
