﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// A <see cref="FlowLayoutPanel"/> which can access the selected <see cref="RadioButton"/>'s index based on its <see cref="Control.Tag"/> property.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.FlowLayoutPanel" />
    [DefaultEvent("SelectedChanged")]
    public class RadioGroupPanel : FlowLayoutPanel
    {
        /// <summary>
        /// Occurs when the selected <see cref="RadioButton"/>'s index changed.
        /// </summary>
        [Description("Occurs when the selected RadioButton changed.")]
        public event EventHandler SelectedChanged;

        /// <summary>
        /// Called when the selected <see cref="RadioButton"/>'s index changed.
        /// </summary>
        protected virtual void OnSelectedChanged(EventArgs e) => this.SelectedChanged?.Invoke(this, e);

        private int _selected;

        /// <summary>
        /// Gets or sets the selected <see cref="RadioButton"/> index.
        /// </summary>
        [Browsable(false)]
        public virtual int Selected
        {
            get => this._selected;
            set
            {
                int val = 0;
                var radioButton = this.Controls.OfType<RadioButton>()
                    .FirstOrDefault(radio => radio.Tag != null && int.TryParse(radio.Tag.ToString(), out val) && val == value);

                if (radioButton != null)
                {
                    radioButton.Checked = true;
                    this._selected = val;
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ControlAdded" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the event data.</param>
        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);

            if (e.Control is RadioButton radioButton)
                radioButton.CheckedChanged += this.radioButton_CheckedChanged;
            else
            {
                //Add children
                foreach (var rb in e.Control.Controls.OfType<RadioButton>())
                {
                    rb.CheckedChanged += this.radioButton_CheckedChanged;
                }
            }
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            var radio = (RadioButton)sender;
            if (radio.Checked && radio.Tag != null && int.TryParse(radio.Tag.ToString(), out var val))
            {
                //FIXME: RadioGroupPanel - do not invoke SelectedChanged when initialized
                this._selected = val;
                this.OnSelectedChanged(EventArgs.Empty);
            }
        }
    }
}
