﻿using System;
using BrightIdeasSoftware;

namespace HammerThemes
{
    /// <summary>
    /// Filter for Unused ThemeColors.
    /// </summary>
    /// <seealso cref="BrightIdeasSoftware.IModelFilter" />
    public class ThemeColorUnusedFilter : IModelFilter
    {
        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            if (modelObject is ThemeColor tc)
                return !tc.Unused;
            return true;
        }
    }
}
