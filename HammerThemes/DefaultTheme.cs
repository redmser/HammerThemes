﻿using System;
using System.Collections.Generic;
using Microsoft.Win32;

namespace HammerThemes
{
    /// <summary>
    /// The default Hammer theme.
    /// </summary>
    [Serializable]
    public class DefaultTheme : Theme
    {
        /// <summary>
        /// Gets or sets the name of the theme.
        /// </summary>
        public override string Name => "(Default Theme)";

        /// <summary>
        /// Applies this theme to Hammer using the registry, by disabling custom colors.
        /// </summary>
        public override void Apply()
        {
            //Disable custom colors
            SetUseCustom(false);

            //Remove all custom color values
            foreach (var color in DefaultColors)
            {
                Registry.CurrentUser.OpenSubKey(Constants.ShortColorsPath, true).DeleteValue(color.Name, false);
                Registry.CurrentUser.OpenSubKey(Constants.OtherShortColorsPath, true).DeleteValue(color.Name, false);
            }

            //Store theme name
            Theme.CurrentTheme = this.Name;
        }

        /// <summary>
        /// Gets the colors used by this theme.
        /// </summary>
        public override List<ThemeColor> Colors => null;
    }
}
