﻿namespace HammerThemes
{
    partial class UpdateAvailable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateAvailable));
            this.updateButton = new System.Windows.Forms.Button();
            this.remindButton = new HammerThemes.MenuButton();
            this.remindContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.remindMeIn1DayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remindMeIn1WeekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remindMeIn1MonthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skipButton = new System.Windows.Forms.Button();
            this.headerLabel = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.changesObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.textOlvColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.label1 = new System.Windows.Forms.Label();
            this.updateProgressBar = new System.Windows.Forms.ProgressBar();
            this.downloadingProgressPanel = new System.Windows.Forms.Panel();
            this.disableAutoUpdatesCheckBox = new System.Windows.Forms.CheckBox();
            this.remindContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.changesObjectListView)).BeginInit();
            this.downloadingProgressPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateButton
            // 
            this.updateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.updateButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.updateButton.Image = global::HammerThemes.Properties.Resources.arrow_go;
            this.updateButton.Location = new System.Drawing.Point(502, 376);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(111, 31);
            this.updateButton.TabIndex = 0;
            this.updateButton.Text = "Update now";
            this.updateButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.updateButton.UseVisualStyleBackColor = true;
            // 
            // remindButton
            // 
            this.remindButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.remindButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.remindButton.DrawSeparationLine = false;
            this.remindButton.Image = global::HammerThemes.Properties.Resources.clock;
            this.remindButton.Location = new System.Drawing.Point(370, 376);
            this.remindButton.Menu = this.remindContextMenuStrip;
            this.remindButton.Name = "remindButton";
            this.remindButton.Size = new System.Drawing.Size(128, 31);
            this.remindButton.TabIndex = 1;
            this.remindButton.Text = "Remind me later";
            this.remindButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.remindButton.UseVisualStyleBackColor = true;
            this.remindButton.Click += new System.EventHandler(this.remindMeIn1DayToolStripMenuItem_Click);
            // 
            // remindContextMenuStrip
            // 
            this.remindContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.remindMeIn1DayToolStripMenuItem,
            this.remindMeIn1WeekToolStripMenuItem,
            this.remindMeIn1MonthToolStripMenuItem});
            this.remindContextMenuStrip.Name = "remindContextMenuStrip";
            this.remindContextMenuStrip.Size = new System.Drawing.Size(180, 70);
            // 
            // remindMeIn1DayToolStripMenuItem
            // 
            this.remindMeIn1DayToolStripMenuItem.Name = "remindMeIn1DayToolStripMenuItem";
            this.remindMeIn1DayToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.remindMeIn1DayToolStripMenuItem.Text = "Remind me in 1 day";
            this.remindMeIn1DayToolStripMenuItem.Click += new System.EventHandler(this.remindMeIn1DayToolStripMenuItem_Click);
            // 
            // remindMeIn1WeekToolStripMenuItem
            // 
            this.remindMeIn1WeekToolStripMenuItem.Name = "remindMeIn1WeekToolStripMenuItem";
            this.remindMeIn1WeekToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.remindMeIn1WeekToolStripMenuItem.Text = "Remind me in 1 week";
            this.remindMeIn1WeekToolStripMenuItem.Click += new System.EventHandler(this.remindMeIn1WeekToolStripMenuItem_Click);
            // 
            // remindMeIn1MonthToolStripMenuItem
            // 
            this.remindMeIn1MonthToolStripMenuItem.Name = "remindMeIn1MonthToolStripMenuItem";
            this.remindMeIn1MonthToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.remindMeIn1MonthToolStripMenuItem.Text = "Remind me in 1 month";
            this.remindMeIn1MonthToolStripMenuItem.Click += new System.EventHandler(this.remindMeIn1MonthToolStripMenuItem_Click);
            // 
            // skipButton
            // 
            this.skipButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.skipButton.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            this.skipButton.Image = global::HammerThemes.Properties.Resources.calendar_view_day;
            this.skipButton.Location = new System.Drawing.Point(12, 376);
            this.skipButton.Name = "skipButton";
            this.skipButton.Size = new System.Drawing.Size(124, 31);
            this.skipButton.TabIndex = 3;
            this.skipButton.Text = "Skip this version";
            this.skipButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.skipButton.UseVisualStyleBackColor = true;
            // 
            // headerLabel
            // 
            this.headerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerLabel.Location = new System.Drawing.Point(16, 12);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(598, 28);
            this.headerLabel.TabIndex = 4;
            this.headerLabel.Text = "A new version of HammerThemes is available!";
            // 
            // versionLabel
            // 
            this.versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(16, 40);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(598, 68);
            this.versionLabel.TabIndex = 5;
            this.versionLabel.Text = "Latest version: {0}\r\nInstalled version: {1}\r\n\r\nWhat\'s changed:";
            // 
            // changesObjectListView
            // 
            this.changesObjectListView.AllColumns.Add(this.textOlvColumn);
            this.changesObjectListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.changesObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.textOlvColumn});
            this.changesObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.changesObjectListView.FullRowSelect = true;
            this.changesObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.changesObjectListView.Location = new System.Drawing.Point(16, 112);
            this.changesObjectListView.Name = "changesObjectListView";
            this.changesObjectListView.RowHeight = 24;
            this.changesObjectListView.ShowGroups = false;
            this.changesObjectListView.Size = new System.Drawing.Size(594, 240);
            this.changesObjectListView.TabIndex = 6;
            this.changesObjectListView.UseCompatibleStateImageBehavior = false;
            this.changesObjectListView.View = System.Windows.Forms.View.Details;
            // 
            // textOlvColumn
            // 
            this.textOlvColumn.AspectName = "Text";
            this.textOlvColumn.FillsFreeSpace = true;
            this.textOlvColumn.Text = "Text";
            this.textOlvColumn.WordWrap = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Downloading:";
            // 
            // updateProgressBar
            // 
            this.updateProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.updateProgressBar.Location = new System.Drawing.Point(76, 2);
            this.updateProgressBar.Name = "updateProgressBar";
            this.updateProgressBar.Size = new System.Drawing.Size(518, 19);
            this.updateProgressBar.TabIndex = 8;
            // 
            // downloadingProgressPanel
            // 
            this.downloadingProgressPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.downloadingProgressPanel.Controls.Add(this.updateProgressBar);
            this.downloadingProgressPanel.Controls.Add(this.label1);
            this.downloadingProgressPanel.Location = new System.Drawing.Point(12, 352);
            this.downloadingProgressPanel.Name = "downloadingProgressPanel";
            this.downloadingProgressPanel.Size = new System.Drawing.Size(598, 24);
            this.downloadingProgressPanel.TabIndex = 9;
            this.downloadingProgressPanel.Visible = false;
            // 
            // disableAutoUpdatesCheckBox
            // 
            this.disableAutoUpdatesCheckBox.AutoSize = true;
            this.disableAutoUpdatesCheckBox.Location = new System.Drawing.Point(140, 384);
            this.disableAutoUpdatesCheckBox.Name = "disableAutoUpdatesCheckBox";
            this.disableAutoUpdatesCheckBox.Size = new System.Drawing.Size(151, 17);
            this.disableAutoUpdatesCheckBox.TabIndex = 10;
            this.disableAutoUpdatesCheckBox.Text = "Disable automatic updates";
            this.disableAutoUpdatesCheckBox.UseVisualStyleBackColor = true;
            // 
            // UpdateAvailable
            // 
            this.AcceptButton = this.updateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.remindButton;
            this.ClientSize = new System.Drawing.Size(623, 419);
            this.Controls.Add(this.disableAutoUpdatesCheckBox);
            this.Controls.Add(this.downloadingProgressPanel);
            this.Controls.Add(this.changesObjectListView);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.headerLabel);
            this.Controls.Add(this.skipButton);
            this.Controls.Add(this.remindButton);
            this.Controls.Add(this.updateButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(396, 280);
            this.Name = "UpdateAvailable";
            this.Text = "Update Available";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UpdateAvailable_FormClosing);
            this.Load += new System.EventHandler(this.UpdateAvailable_Load);
            this.remindContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.changesObjectListView)).EndInit();
            this.downloadingProgressPanel.ResumeLayout(false);
            this.downloadingProgressPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateButton;
        private HammerThemes.MenuButton remindButton;
        private System.Windows.Forms.ContextMenuStrip remindContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem remindMeIn1DayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remindMeIn1WeekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remindMeIn1MonthToolStripMenuItem;
        private System.Windows.Forms.Button skipButton;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.Label versionLabel;
        private BrightIdeasSoftware.ObjectListView changesObjectListView;
        private BrightIdeasSoftware.OLVColumn textOlvColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar updateProgressBar;
        private System.Windows.Forms.Panel downloadingProgressPanel;
        private System.Windows.Forms.CheckBox disableAutoUpdatesCheckBox;
    }
}