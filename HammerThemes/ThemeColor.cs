﻿using System;
using System.Drawing;

namespace HammerThemes
{
    /// <summary>
    /// A color entry in the theme.
    /// </summary>
    [Serializable]
    public class ThemeColor : ICloneable
    {
        /// <summary>
        /// Gets or sets whether this color override is enabled.
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the display name of this color. If set to null (default), shows the key name.
        /// </summary>
        public string Text
        {
            get
            {
                if (this._text == null)
                    return this.Name;
                return this._text;
            }
            set => this._text = value;
        }
        [NonSerialized]
        private string _text;

        /// <summary>
        /// Gets or sets the name of the registry key to set for the color value.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a short description on what element in Hammer gets recolored by this ThemeColor.
        /// </summary>
        public string Description
        {
            get
            {
                if (this.Unused || this._description == null)
                    return Constants.UnusedText;
                return this._description;
            }
            set => this._description = value;
        }
        [NonSerialized]
        private string _description;

        /// <summary>
        /// Gets or sets the color value.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Gets or sets the string representation of this color, used as the value for the registry entry.
        /// </summary>
        public string ColorString
        {
            get => $"{this.Color.R} {this.Color.G} {this.Color.B}";
            set
            {
                var split = value.Split(' ');
                this.Color = Color.FromArgb(byte.Parse(split[0]), byte.Parse(split[1]), byte.Parse(split[2]));
            }
        }

        /// <summary>
        /// Gets or sets whether this ThemeColor is unused or unknown by Hammer.
        /// </summary>
        public bool Unused
        {
            get => this._unused;
            set => this._unused = value;
        }
        [NonSerialized]
        private bool _unused;

        /// <summary>
        /// Gets or sets whether this color scales with the Grid Intensity setting of Hammer.
        /// </summary>
        public bool Scales { get; set; }

        /// <summary>
        /// Gets or sets whether this color can be scaled by the Grid Intensity setting of Hammer.
        /// </summary>
        public bool CanScale
        {
            get => this._canscale;
            set => this._canscale = value;
        }
        [NonSerialized]
        private bool _canscale;

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{this.Text} = {this.ColorString}";

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            var tc = new ThemeColor();
            tc.Name = this.Name;
            tc.Text = this.Text;
            tc.Description = this.Description;
            tc.Color = this.Color;
            tc.Enabled = this.Enabled;
            tc.CanScale = this.CanScale;
            tc.Scales = this.Scales;
            tc.Unused = this.Unused;
            return tc;
        }
    }
}
