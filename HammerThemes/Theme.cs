﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Win32;
using Newtonsoft.Json.Linq;
using System.Drawing;

namespace HammerThemes
{
    /// <summary>
    /// A theme used by Hammer.
    /// </summary>
    [Serializable]
    public class Theme
    {
        /// <summary>
        /// Gets or sets the name of the theme.
        /// </summary>
        public virtual string Name
        {
            get => this._name;
            set
            {
                if (this._name == value)
                    return;

                this._name = value;
                this.OnNameChanged(EventArgs.Empty);
            }
        }
        private string _name;

        /// <summary>
        /// Occurs when the name of the theme changed.
        /// </summary>
        [field: NonSerialized]
        public event EventHandler NameChanged;

        /// <summary>
        /// Raises the <see cref="E:NameChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnNameChanged(EventArgs e) => this.NameChanged?.Invoke(this, e);

        /// <summary>
        /// Gets the colors used by this theme.
        /// </summary>
        public virtual List<ThemeColor> Colors { get; protected set; }

        /// <summary>
        /// Gets the color with the given internal name.
        /// </summary>
        /// <param name="name">The name of the color (registry name).</param>
        public ThemeColor GetColor(string name)
        {
            var color = (this.Colors ?? Theme.DefaultColors).FirstOrDefault(c => c.Name == name);
            return color;
        }

        /// <summary>
        /// Applies this theme to Hammer using the registry.
        /// </summary>
        public virtual void Apply()
        {
            //Enable theming
            SetUseCustom(true);

            //Apply colors
            foreach (var color in this.Colors)
            {
                if (color.Enabled)
                    SetColorValue(color);
                else
                    SetColorValue(color.Name, DefaultColors.FirstOrDefault(c => c.Name == color.Name).ColorString);
            }

            //Store theme name
            Theme.CurrentTheme = this.Name;
        }

        /// <summary>
        /// Initializes the list of ThemeColors by loading the list from the configuration.
        /// </summary>
        public virtual void InitializeColors()
        {
            this.Colors = CreateClone(DefaultColors).ToList();



            IEnumerable<T> CreateClone<T>(IEnumerable<T> input)
            {
                foreach (var i in input)
                {
                    if (i is ICloneable icl)
                    {
                        yield return (T)icl.Clone();
                    }
                    else
                        throw new ArgumentException($"Type {i.GetType().Name} is not cloneable!");
                }
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => this.Name;

        /// <summary>
        /// Gets the list of default colors, initialized once needed.
        /// </summary>
        public static List<ThemeColor> DefaultColors { get; private set; }

        /// <summary>
        /// Populates the list of default colors.
        /// </summary>
        public static void PopulateDefaultColors()
        {
            //Initialize colors
            var obj = JObject.Parse(Theme.ReadColors());
            var objs = obj.Value<JArray>("ThemeColors");
            var themes = objs.ToObject<List<ThemeColor>>();
            DefaultColors = themes.ToList();
        }

        /// <summary>
        /// Returns the contents of the ThemeColors config file.
        /// </summary>
        /// <returns></returns>
        protected static string ReadColors()
        {
            using (var file = File.OpenRead(Constants.ThemeColors))
            using (var stream = new StreamReader(file))
            {
                return stream.ReadToEnd();
            }
        }

        /// <summary>
        /// Sets the UseCustom property in the registry.
        /// </summary>
        /// <param name="value"></param>
        protected static void SetUseCustom(bool value)
        {
            Registry.SetValue(Constants.ColorsPath, Constants.UseCustom, value ? 1 : 0);
            Registry.SetValue(Constants.OtherColorsPath, Constants.UseCustom, value ? 1 : 0);
        }

        /// <summary>
        /// Sets the specified color value in the registry.
        /// </summary>
        /// <param name="color">The color value to set.</param>
        protected static void SetColorValue(ThemeColor color)
        {
            SetColorValue(color.Name, color.ColorString);

            //Scale variable?
            if (color.CanScale)
                SetScalesValue("Scale" + color.Name, color.Scales);
        }

        /// <summary>
        /// Sets the specified scaling intensity value in the registry.
        /// </summary>
        /// <param name="name">Name of the scaling value.</param>
        /// <param name="scales">Value to set.</param>
        protected static void SetScalesValue(string name, bool scales)
        {
            Registry.SetValue(Constants.ColorsPath, name, scales ? 1 : 0);
            Registry.SetValue(Constants.OtherColorsPath, name, scales ? 1 : 0);
        }

        /// <summary>
        /// Sets the specified color value in the registry.
        /// </summary>
        /// <param name="name">Name of the color value.</param>
        /// <param name="color">The color to set, as an R G B string.</param>
        protected static void SetColorValue(string name, string color)
        {
            Registry.SetValue(Constants.ColorsPath, name, color);
            Registry.SetValue(Constants.OtherColorsPath, name, color);
        }

        /// <summary>
        /// Gets or sets the currently enabled Hammer theme, by name.
        /// </summary>
        public static string CurrentTheme
        {
            get => Registry.GetValue(Constants.ColorsPath, Constants.CurrentTheme, "(Default Theme)").ToString();
            set => Registry.SetValue(Constants.ColorsPath, Constants.CurrentTheme, value);
        }

        /// <summary>
        /// Returns a new name for a theme, given the existing list of themes.
        /// </summary>
        /// <param name="existingNames">Themes list.</param>
        /// <returns></returns>
        public static string NewThemeName(IEnumerable<string> existingNames)
        {
            //Find the name with the highest number
            var names = existingNames.Where(s => s != null && (s.StartsWith("THEME", StringComparison.InvariantCultureIgnoreCase) &&
                !s.Equals("THEME", StringComparison.InvariantCultureIgnoreCase))).Select(s => new string(s.Where(c => char.IsDigit(c)).ToArray()));
            if (!names.Any())
                return "Theme1";

            var highest = names.Max(s => int.Parse(s));
            return "Theme" + (highest + 1);
        }

        /// <summary>
        /// Loads the specified theme from file.
        /// </summary>
        /// <param name="fileName">File path.</param>
        /// <returns></returns>
        public static Theme LoadTheme(string fileName)
        {
            //Deserialize theme from file
            var ser = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (var file = File.OpenRead(fileName))
            {
                var theme = (Theme)ser.Deserialize(file);

                //Copy non-serialized properties
                foreach (var tc in theme.Colors)
                {
                    var corr = Theme.DefaultColors.FirstOrDefault(c => c.Name == tc.Name);
                    if (corr == null)
                        continue;
                    tc.Description = corr.Description;
                    tc.Text = corr.Text;
                    tc.Unused = corr.Unused;
                    tc.CanScale = corr.CanScale;
                }
                return theme;
            }
        }

        /// <summary>
        /// Saves this theme to file.
        /// </summary>
        /// <param name="fileName">File path.</param>
        /// <returns></returns>
        public void SaveTheme(string fileName)
        {
            //Serialize theme to file
            var ser = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (var file = File.OpenWrite(fileName))
            {
                ser.Serialize(file, this);
            }
        }
    }
}
