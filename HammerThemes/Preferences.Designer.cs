﻿namespace HammerThemes
{
    partial class Preferences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Preferences));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl1 = new HammerThemes.InvisibleControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkingLabel = new System.Windows.Forms.Label();
            this.updateCheckButton = new System.Windows.Forms.Button();
            this.updateAvailableRadioGroupPanel = new HammerThemes.RadioGroupPanel();
            this.notifyRadioButton = new System.Windows.Forms.RadioButton();
            this.downloadRadioButton = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.updateFrequencyComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.updateAvailableRadioGroupPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.okButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cancelButton, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 288);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(406, 32);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(250, 2);
            this.okButton.Margin = new System.Windows.Forms.Padding(2);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 26);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(329, 2);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 26);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.invisibleControl1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(406, 284);
            this.flowLayoutPanel1.TabIndex = 2;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // invisibleControl1
            // 
            this.invisibleControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl1.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl1.Name = "invisibleControl1";
            this.invisibleControl1.Size = new System.Drawing.Size(406, 0);
            this.invisibleControl1.TabIndex = 0;
            this.invisibleControl1.TabStop = false;
            this.invisibleControl1.Text = "invisibleControl1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkingLabel);
            this.groupBox1.Controls.Add(this.updateCheckButton);
            this.groupBox1.Controls.Add(this.updateAvailableRadioGroupPanel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.updateFrequencyComboBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 169);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Updating";
            // 
            // checkingLabel
            // 
            this.checkingLabel.AutoSize = true;
            this.checkingLabel.Location = new System.Drawing.Point(128, 140);
            this.checkingLabel.Name = "checkingLabel";
            this.checkingLabel.Size = new System.Drawing.Size(117, 13);
            this.checkingLabel.TabIndex = 5;
            this.checkingLabel.Text = "Checking for updates...";
            this.checkingLabel.Visible = false;
            // 
            // updateCheckButton
            // 
            this.updateCheckButton.Location = new System.Drawing.Point(8, 132);
            this.updateCheckButton.Name = "updateCheckButton";
            this.updateCheckButton.Size = new System.Drawing.Size(116, 27);
            this.updateCheckButton.TabIndex = 4;
            this.updateCheckButton.Text = "Check for &updates";
            this.updateCheckButton.UseVisualStyleBackColor = true;
            this.updateCheckButton.Click += new System.EventHandler(this.updateCheckButton_Click);
            // 
            // updateAvailableRadioGroupPanel
            // 
            this.updateAvailableRadioGroupPanel.Controls.Add(this.notifyRadioButton);
            this.updateAvailableRadioGroupPanel.Controls.Add(this.downloadRadioButton);
            this.updateAvailableRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.updateAvailableRadioGroupPanel.Location = new System.Drawing.Point(32, 76);
            this.updateAvailableRadioGroupPanel.Name = "updateAvailableRadioGroupPanel";
            this.updateAvailableRadioGroupPanel.Selected = 0;
            this.updateAvailableRadioGroupPanel.Size = new System.Drawing.Size(164, 48);
            this.updateAvailableRadioGroupPanel.TabIndex = 3;
            // 
            // notifyRadioButton
            // 
            this.notifyRadioButton.AutoSize = true;
            this.notifyRadioButton.Checked = true;
            this.notifyRadioButton.Location = new System.Drawing.Point(3, 3);
            this.notifyRadioButton.Name = "notifyRadioButton";
            this.notifyRadioButton.Size = new System.Drawing.Size(52, 17);
            this.notifyRadioButton.TabIndex = 2;
            this.notifyRadioButton.TabStop = true;
            this.notifyRadioButton.Tag = "0";
            this.notifyRadioButton.Text = "Notify";
            this.notifyRadioButton.UseVisualStyleBackColor = true;
            // 
            // downloadRadioButton
            // 
            this.downloadRadioButton.AutoSize = true;
            this.downloadRadioButton.Location = new System.Drawing.Point(3, 26);
            this.downloadRadioButton.Name = "downloadRadioButton";
            this.downloadRadioButton.Size = new System.Drawing.Size(158, 17);
            this.downloadRadioButton.TabIndex = 0;
            this.downloadRadioButton.Tag = "1";
            this.downloadRadioButton.Text = "Download update and notify";
            this.downloadRadioButton.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "When an update is available:";
            // 
            // updateFrequencyComboBox
            // 
            this.updateFrequencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.updateFrequencyComboBox.FormattingEnabled = true;
            this.updateFrequencyComboBox.Items.AddRange(new object[] {
            "Never",
            "On launch",
            "Daily",
            "Weekly",
            "Monthly"});
            this.updateFrequencyComboBox.Location = new System.Drawing.Point(176, 20);
            this.updateFrequencyComboBox.Name = "updateFrequencyComboBox";
            this.updateFrequencyComboBox.Size = new System.Drawing.Size(124, 21);
            this.updateFrequencyComboBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Automatically check for updates:";
            // 
            // Preferences
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(414, 324);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(337, 273);
            this.Name = "Preferences";
            this.Padding = new System.Windows.Forms.Padding(4);
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Preferences_FormClosing);
            this.Load += new System.EventHandler(this.Preferences_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.updateAvailableRadioGroupPanel.ResumeLayout(false);
            this.updateAvailableRadioGroupPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private InvisibleControl invisibleControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox updateFrequencyComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button updateCheckButton;
        private RadioGroupPanel updateAvailableRadioGroupPanel;
        private System.Windows.Forms.RadioButton notifyRadioButton;
        private System.Windows.Forms.RadioButton downloadRadioButton;
        private System.Windows.Forms.Label checkingLabel;
    }
}