﻿using System.Collections.Generic;
using System.Configuration;

namespace HammerThemes.Properties {
    
    
    // This class allows you to handle specific events on the settings class:
    //  The SettingChanging event is raised before a setting's value is changed.
    //  The PropertyChanged event is raised after a setting's value is changed.
    //  The SettingsLoaded event is raised after the setting values are loaded.
    //  The SettingsSaving event is raised before the setting values are saved.
    internal sealed partial class Settings
    {
        public Settings()
        {
            // // To add event handlers for saving and changing settings, uncomment the lines below:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
        }
        
        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            // Add code to handle the SettingChangingEvent event here.
        }
        
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Add code to handle the SettingsSaving event here.
        }

        /// <summary>
        /// Gets or sets the list of themes.
        /// </summary>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<Theme> Themes
        {
            get => (List<Theme>)this["Themes"];
            set => this["Themes"] = value;
        }

        /// <summary>
        /// Gets or sets the state of the colors OLV.
        /// </summary>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public byte[] OLVState
        {
            get => (byte[])this["OLVState"];
            set => this["OLVState"] = value;
        }

        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public GoodRectangle PreviewBrush
        {
            get => (GoodRectangle)this["PreviewBrush"];
            set => this["PreviewBrush"] = value;
        }
    }
}
