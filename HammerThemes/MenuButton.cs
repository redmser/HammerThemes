﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// A button with a menu arrow that allows you to run extra tasks.
    /// </summary>
    public class MenuButton : Button
    {
        //TODO: MenuButton - holding down leftclick does not allow user to instantly select menu entry on mouse button release (unlike normal menus)
        //TODO: MenuButton - property (true by default) to hide the menu when clicking the button a second time

        private ContextMenuStrip _menu;
        private bool _drawSeparationLine;

        /// <summary>
        /// Specifies the ContextMenuStrip to show when the button's menu should show. When set to null, the arrow is not displayed.
        /// </summary>
        [DefaultValue(null), Category("Menu"), Description("Specifies the ContextMenuStrip to show when the button's menu should show.")]
        public virtual ContextMenuStrip Menu
        {
            get { return this._menu; }
            set
            {
                if (this._menu == value)
                    return;

                if (this.Menu != null)
                    this.Menu.ItemClicked -= this.OnMenuItemClicked;

                this._menu = value;

                if (this.Menu != null)
                    this.Menu.ItemClicked += this.OnMenuItemClicked;
                this.Invalidate(); //Re-render to show/hide the arrow
            }
        }

        /// <summary>
        /// Whether to show the menu under the cursor or under the button.
        /// </summary>
        [DefaultValue(false), Category("Menu"), Description("Whether to show the menu under the cursor or under the button.")]
        public virtual bool ShowMenuUnderCursor { get; set; }

        /// <summary>
        /// Should the menu only show if the arrow is clicked, or simply by pressing the button?
        /// </summary>
        [DefaultValue(true), Category("Menu"), Description("Whether the button should be split or always act as a menu.")]
        public virtual bool SplitButton { get; set; }

        /// <summary>
        /// Whether the menu being displayed (when clicking on the arrow only) should cancel any Click event from being raised.
        /// </summary>
        [DefaultValue(true), Category("Menu"), Description("Whether the menu being opened should cancel a click event from being raised.")]
        public virtual bool MenuCancelsClick { get; set; }

        /// <summary>
        /// Whether clicking a menu entry should raise the button's click event as well.
        /// </summary>
        [DefaultValue(false), Category("Menu"), Description("Whether clicking a menu entry should raise the button's click event as well.")]
        public virtual bool MenuRaisesButtonClick { get; set; }

        /// <summary>
        /// Whether the arrow and regularly clickable area of the button should be separated by a line. Recommended if you use the SplitButton property.
        /// </summary>
        [DefaultValue(true), Category("Menu"), Description("Whether the arrow and regularly clickable area of the button should be separated by a line.")]
        public virtual bool DrawSeparationLine
        {
            get { return this._drawSeparationLine; }
            set
            {
                if (this._drawSeparationLine == value)
                    return;

                this._drawSeparationLine = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Whether the upcoming click is an automated click event and should be ignored.
        /// </summary>
        protected bool automatedClick;

        /// <summary>
        /// Whether to cancel the upcoming click events when pressing the menu portion.
        /// </summary>
        protected bool cancelClick = false;

        private int _splitAreaSize = 20;

        /// <summary>
        /// Size of the area where the split button is clickable.
        /// </summary>
        [DefaultValue(20), Category("Menu"), Description("Size of the area where the split button is clickable.")]
        public virtual int SplitAreaSize
        {
            //FIXME: MenuButton - Instead of clamping a too low value, try to scale the arrow on smaller sizes
            get { return this.FlatStyle == FlatStyle.Standard ? Math.Max(10, this._splitAreaSize) : this._splitAreaSize; }
            set
            {
                if (this._splitAreaSize == value)
                    return;

                this._splitAreaSize = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:MenuItemClicked" /> event.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The <see cref="ToolStripItemClickedEventArgs" /> instance containing the event data.</param>
        protected virtual void OnMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (this.MenuRaisesButtonClick)
            {
                this.automatedClick = true;
                this.PerformClick();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnClick(EventArgs e)
        {
            if (!this.cancelClick)
            {
                base.OnClick(e);
            }
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" /> event.
        /// </summary>
        /// <param name="mevent">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            this.cancelClick = false;

            base.OnMouseDown(mevent);

            if (this.Menu != null && mevent.Button == MouseButtons.Left && (!this.SplitButton || mevent.Location.X >= this.ClientRectangle.Width - this.SplitAreaSize))
            {
                var menuLocation = this.ShowMenuUnderCursor ? mevent.Location : new Point(0, this.Height);

                this.Menu.Show(this, menuLocation);

                if (this.MenuCancelsClick)
                    this.cancelClick = true;
            }
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" /> event.
        /// </summary>
        /// <param name="pevent">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);

            if (!this.DesignMode && this.Menu != null)
            {
                var arrowX = this.ClientRectangle.Width - this.SplitAreaSize / 2 - 5; //Middle of the split area
                var arrowY = this.ClientRectangle.Height / 2 - 1; //Middle of the button

                //FIXME: MenuButton - correct theming of the dropdown arrow
                var brush = this.Enabled ? SystemBrushes.ControlText : SystemBrushes.ButtonShadow;
                var arrows = new[] { new Point(arrowX, arrowY), new Point(arrowX + 7, arrowY), new Point(arrowX + 3, arrowY + 4) };
                pevent.Graphics.FillPolygon(brush, arrows);

                if (this.DrawSeparationLine)
                {
                    var topmargin = this.FlatStyle == FlatStyle.Standard ? 2 : 0;
                    var bottommargin = this.FlatStyle == FlatStyle.Standard ? 4 : 0;
                    pevent.Graphics.DrawLine(new Pen(brush, 1), this.ClientRectangle.Width - this.SplitAreaSize, topmargin, this.ClientRectangle.Width - this.SplitAreaSize, this.ClientRectangle.Height - bottommargin);
                }
            }
        }
    }
}
