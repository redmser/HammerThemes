﻿namespace HammerThemes
{
    /// <summary>
    /// Container for constant values.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Gets a registry path to the custom theme colors used by Hammer.
        /// </summary>
        public const string ShortColorsPath = @"Software\Valve\Hammer\Custom2DColors";

        /// <summary>
        /// Gets a full registry path to the custom theme colors used by Hammer.
        /// </summary>
        public const string ColorsPath = @"HKEY_CURRENT_USER\" + ShortColorsPath;

        /// <summary>
        /// Gets another registry path to the custom theme colors used by Hammer.
        /// </summary>
        public const string OtherShortColorsPath = @"Software\Valve\Valve Hammer Editor\Custom2DColors";

        /// <summary>
        /// Gets another full registry path to the custom theme colors used by Hammer.
        /// </summary>
        public const string OtherColorsPath = @"HKEY_CURRENT_USER\" + OtherShortColorsPath;

        /// <summary>
        /// Gets the registry key name "UseCustom" for Hammer themes.
        /// </summary>
        public const string UseCustom = "UseCustom";

        /// <summary>
        /// Gets the registry key name for the name of the currently selected Hammer theme.
        /// </summary>
        public const string CurrentTheme = "CurrentTheme";

        /// <summary>
        /// Gets the config file for the list of ThemeColors.
        /// </summary>
        public const string ThemeColors = "ThemeColors.json";

        /// <summary>
        /// Gets a string for unknown/unused color keys.
        /// </summary>
        public const string UnusedText = "Unused by the latest version of Hammer Editor!";

        /// <summary>
        /// Gets a URL for my paypal donation page.
        /// </summary>
        public const string DonatePaypalURL = @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=B7K6KJ3VECKKG&lc=US";

        /// <summary>
        /// Gets a URL for my bitcoin donation page.
        /// </summary>
        public const string DonateBitcoinURL = @"https://blockchain.info/payment_request?address=" + DonateBitcoinAddress + "&message=Donations%20to%20RedMser";

        /// <summary>
        /// Gets my bitcoin wallet address.
        /// </summary>
        public const string DonateBitcoinAddress = @"1JdK6dMdBU19c2jSCSqWcSKMAJk2T8RXqU";
    }
}
