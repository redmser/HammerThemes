﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// A dialog box for selecting the preferred donation method.
    /// </summary>
    public partial class Donate : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Donate"/> class.
        /// </summary>
        public Donate()
        {
            //Compiler-generated
            InitializeComponent();

            //Give text
            this.addressTextBox.Text = Constants.DonateBitcoinAddress;
        }

        private void Donate_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
        }

        private void paypalButton_Click(object sender, EventArgs e)
        {
            Process.Start(Constants.DonatePaypalURL);
        }

        private void bitcoinButton_Click(object sender, EventArgs e)
        {
            //Show help if no worky
            this.label2.Text = "Thank you for your interest in supporting the development of my software!\r\n\r\nIf the link does not work, you can find my Bitcoin address here:";
            this.addressTextBox.Visible = true;

            Process.Start(Constants.DonateBitcoinURL);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addressTextBoxDarkTheme_Click(object sender, EventArgs e)
        {
            //Select only
            this.addressTextBox.SelectAll();
        }

        private void addressTextBoxDarkTheme_DoubleClick(object sender, EventArgs e)
        {
            //Copy to clipboard
            Clipboard.SetText(this.addressTextBox.Text);
        }
    }
}
