﻿namespace HammerThemes
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.colorNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colorDescriptionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.themeLabel = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.unusedCheckBox = new System.Windows.Forms.CheckBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.exportButton = new System.Windows.Forms.Button();
            this.importButton = new System.Windows.Forms.Button();
            this.themesComboBox = new System.Windows.Forms.ComboBox();
            this.applyButton = new System.Windows.Forms.Button();
            this.newButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.cloneButton = new System.Windows.Forms.Button();
            this.aboutButton = new System.Windows.Forms.Button();
            this.donateButton = new System.Windows.Forms.Button();
            this.footerSplitContainer = new System.Windows.Forms.SplitContainer();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.helpLabel = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.colorsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.colorTextColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colorColorColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colorValueColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colorIntensityColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colorsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.previewPanel = new HammerThemes.PreviewPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.dottedGridCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gridSizeTrackBar = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.gridIntensityTrackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.settingsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.footerSplitContainer)).BeginInit();
            this.footerSplitContainer.Panel1.SuspendLayout();
            this.footerSplitContainer.Panel2.SuspendLayout();
            this.footerSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorsObjectListView)).BeginInit();
            this.colorsContextMenuStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSizeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridIntensityTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // colorNameColumn
            // 
            this.colorNameColumn.AspectName = "Name";
            this.colorNameColumn.CellPadding = null;
            this.colorNameColumn.IsEditable = false;
            this.colorNameColumn.IsVisible = false;
            this.colorNameColumn.Text = "Registry Key";
            this.colorNameColumn.UseFiltering = false;
            // 
            // colorDescriptionColumn
            // 
            this.colorDescriptionColumn.AspectName = "Description";
            this.colorDescriptionColumn.CellPadding = null;
            this.colorDescriptionColumn.IsEditable = false;
            this.colorDescriptionColumn.IsVisible = false;
            this.colorDescriptionColumn.Text = "Description";
            this.colorDescriptionColumn.UseFiltering = false;
            // 
            // themeLabel
            // 
            this.themeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.themeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.themeLabel.Location = new System.Drawing.Point(2, 0);
            this.themeLabel.Name = "themeLabel";
            this.themeLabel.Size = new System.Drawing.Size(602, 32);
            this.themeLabel.TabIndex = 7;
            this.themeLabel.Text = "Current Theme: Default";
            this.themeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // unusedCheckBox
            // 
            this.unusedCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.unusedCheckBox.AutoSize = true;
            this.unusedCheckBox.Checked = global::HammerThemes.Properties.Settings.Default.ShowUnused;
            this.unusedCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::HammerThemes.Properties.Settings.Default, "ShowUnused", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.unusedCheckBox.Location = new System.Drawing.Point(12, 406);
            this.unusedCheckBox.Name = "unusedCheckBox";
            this.unusedCheckBox.Size = new System.Drawing.Size(122, 17);
            this.unusedCheckBox.TabIndex = 5;
            this.unusedCheckBox.Text = "Show &unused colors";
            this.toolTip.SetToolTip(this.unusedCheckBox, "Show or hide any colors which are unused by newer versions of Hammer editor");
            this.unusedCheckBox.UseVisualStyleBackColor = true;
            this.unusedCheckBox.CheckedChanged += new System.EventHandler(this.unusedCheckBox_CheckedChanged);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.Location = new System.Drawing.Point(48, 18);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(325, 20);
            this.nameTextBox.TabIndex = 2;
            this.toolTip.SetToolTip(this.nameTextBox, "The name of this theme");
            // 
            // exportButton
            // 
            this.exportButton.Location = new System.Drawing.Point(304, 28);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(75, 23);
            this.exportButton.TabIndex = 9;
            this.exportButton.Text = "&Export...";
            this.exportButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.exportButton, "Saved the selected theme to file");
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
            // 
            // importButton
            // 
            this.importButton.Location = new System.Drawing.Point(228, 28);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(75, 23);
            this.importButton.TabIndex = 8;
            this.importButton.Text = "&Import...";
            this.importButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.importButton, "Creates a new theme from a theme file");
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);
            // 
            // themesComboBox
            // 
            this.themesComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.themesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.themesComboBox.FormattingEnabled = true;
            this.themesComboBox.Location = new System.Drawing.Point(48, 2);
            this.themesComboBox.Name = "themesComboBox";
            this.themesComboBox.Size = new System.Drawing.Size(329, 21);
            this.themesComboBox.TabIndex = 3;
            this.toolTip.SetToolTip(this.themesComboBox, "Select a theme for editing or applying");
            this.themesComboBox.SelectionChangeCommitted += new System.EventHandler(this.themesComboBox_SelectionChangeCommitted);
            // 
            // applyButton
            // 
            this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.applyButton.Image = global::HammerThemes.Properties.Resources.tick;
            this.applyButton.Location = new System.Drawing.Point(792, 2);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(93, 29);
            this.applyButton.TabIndex = 1;
            this.applyButton.Text = "&Apply Theme";
            this.applyButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.applyButton, "Applies the currently selected theme to Hammer editor. You will have to restart H" +
        "ammer for any changes to occur!");
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // newButton
            // 
            this.newButton.Image = global::HammerThemes.Properties.Resources.add;
            this.newButton.Location = new System.Drawing.Point(0, 28);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(75, 23);
            this.newButton.TabIndex = 4;
            this.newButton.Text = "&New";
            this.newButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.newButton, "Create a new theme based off the default theme");
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Image = global::HammerThemes.Properties.Resources.delete;
            this.deleteButton.Location = new System.Drawing.Point(152, 28);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.Text = "&Delete";
            this.deleteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.deleteButton, "Deletes the selected theme");
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // cloneButton
            // 
            this.cloneButton.Image = global::HammerThemes.Properties.Resources.arrow_divide;
            this.cloneButton.Location = new System.Drawing.Point(76, 28);
            this.cloneButton.Name = "cloneButton";
            this.cloneButton.Size = new System.Drawing.Size(75, 23);
            this.cloneButton.TabIndex = 5;
            this.cloneButton.Text = "&Clone";
            this.cloneButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.cloneButton, "Create a new theme, copying the values of the selected theme");
            this.cloneButton.UseVisualStyleBackColor = true;
            this.cloneButton.Click += new System.EventHandler(this.cloneButton_Click);
            // 
            // aboutButton
            // 
            this.aboutButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aboutButton.Image = global::HammerThemes.Properties.Resources.help;
            this.aboutButton.Location = new System.Drawing.Point(604, 2);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(29, 29);
            this.aboutButton.TabIndex = 12;
            this.aboutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.aboutButton, "Check out what this is and who made it possible");
            this.aboutButton.UseVisualStyleBackColor = true;
            this.aboutButton.Click += new System.EventHandler(this.aboutButton_Click);
            // 
            // donateButton
            // 
            this.donateButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.donateButton.Image = global::HammerThemes.Properties.Resources.heart;
            this.donateButton.Location = new System.Drawing.Point(632, 2);
            this.donateButton.Name = "donateButton";
            this.donateButton.Size = new System.Drawing.Size(80, 29);
            this.donateButton.TabIndex = 10;
            this.donateButton.Text = "D&onate";
            this.donateButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.donateButton, "If you like this program, consider donating! <3");
            this.donateButton.UseVisualStyleBackColor = true;
            this.donateButton.Click += new System.EventHandler(this.donateButton_Click);
            // 
            // footerSplitContainer
            // 
            this.footerSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.footerSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.footerSplitContainer.IsSplitterFixed = true;
            this.footerSplitContainer.Location = new System.Drawing.Point(8, 8);
            this.footerSplitContainer.Name = "footerSplitContainer";
            this.footerSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // footerSplitContainer.Panel1
            // 
            this.footerSplitContainer.Panel1.Controls.Add(this.mainSplitContainer);
            // 
            // footerSplitContainer.Panel2
            // 
            this.footerSplitContainer.Panel2.Controls.Add(this.aboutButton);
            this.footerSplitContainer.Panel2.Controls.Add(this.settingsButton);
            this.footerSplitContainer.Panel2.Controls.Add(this.applyButton);
            this.footerSplitContainer.Panel2.Controls.Add(this.donateButton);
            this.footerSplitContainer.Panel2.Controls.Add(this.themeLabel);
            this.footerSplitContainer.Size = new System.Drawing.Size(886, 575);
            this.footerSplitContainer.SplitterDistance = 538;
            this.footerSplitContainer.TabIndex = 12;
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mainSplitContainer.DataBindings.Add(new System.Windows.Forms.Binding("SplitterDistance", global::HammerThemes.Properties.Settings.Default, "MainSplitDistance", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.label2);
            this.mainSplitContainer.Panel1.Controls.Add(this.groupBox);
            this.mainSplitContainer.Panel1.Controls.Add(this.exportButton);
            this.mainSplitContainer.Panel1.Controls.Add(this.importButton);
            this.mainSplitContainer.Panel1.Controls.Add(this.themesComboBox);
            this.mainSplitContainer.Panel1.Controls.Add(this.newButton);
            this.mainSplitContainer.Panel1.Controls.Add(this.deleteButton);
            this.mainSplitContainer.Panel1.Controls.Add(this.cloneButton);
            this.mainSplitContainer.Panel1MinSize = 385;
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.panel1);
            this.mainSplitContainer.Panel2.Controls.Add(this.label3);
            this.mainSplitContainer.Panel2MinSize = 110;
            this.mainSplitContainer.Size = new System.Drawing.Size(886, 538);
            this.mainSplitContainer.SplitterDistance = global::HammerThemes.Properties.Settings.Default.MainSplitDistance;
            this.mainSplitContainer.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Themes:";
            // 
            // groupBox
            // 
            this.groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox.Controls.Add(this.unusedCheckBox);
            this.groupBox.Controls.Add(this.helpLabel);
            this.groupBox.Controls.Add(this.titleLabel);
            this.groupBox.Controls.Add(this.nameTextBox);
            this.groupBox.Controls.Add(this.label1);
            this.groupBox.Controls.Add(this.colorsObjectListView);
            this.groupBox.Location = new System.Drawing.Point(0, 56);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(381, 478);
            this.groupBox.TabIndex = 0;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Theme Settings";
            // 
            // helpLabel
            // 
            this.helpLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.helpLabel.Location = new System.Drawing.Point(8, 446);
            this.helpLabel.Name = "helpLabel";
            this.helpLabel.Size = new System.Drawing.Size(367, 26);
            this.helpLabel.TabIndex = 4;
            this.helpLabel.Text = "Select a color to get help for it.";
            // 
            // titleLabel
            // 
            this.titleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(8, 430);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(107, 13);
            this.titleLabel.TabIndex = 3;
            this.titleLabel.Text = "No color selected";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name:";
            // 
            // colorsObjectListView
            // 
            this.colorsObjectListView.AllColumns.Add(this.colorTextColumn);
            this.colorsObjectListView.AllColumns.Add(this.colorNameColumn);
            this.colorsObjectListView.AllColumns.Add(this.colorColorColumn);
            this.colorsObjectListView.AllColumns.Add(this.colorValueColumn);
            this.colorsObjectListView.AllColumns.Add(this.colorDescriptionColumn);
            this.colorsObjectListView.AllColumns.Add(this.colorIntensityColumn);
            this.colorsObjectListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.colorsObjectListView.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.SingleClick;
            this.colorsObjectListView.CellEditTabChangesRows = true;
            this.colorsObjectListView.CheckedAspectName = "Enabled";
            this.colorsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colorTextColumn,
            this.colorColorColumn,
            this.colorValueColumn,
            this.colorIntensityColumn});
            this.colorsObjectListView.ContextMenuStrip = this.colorsContextMenuStrip;
            this.colorsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.colorsObjectListView.HideSelection = false;
            this.colorsObjectListView.Location = new System.Drawing.Point(8, 44);
            this.colorsObjectListView.Name = "colorsObjectListView";
            this.colorsObjectListView.RenderNonEditableCheckboxesAsDisabled = true;
            this.colorsObjectListView.ShowGroups = false;
            this.colorsObjectListView.ShowImagesOnSubItems = true;
            this.colorsObjectListView.Size = new System.Drawing.Size(365, 355);
            this.colorsObjectListView.TabIndex = 0;
            this.colorsObjectListView.UseCellFormatEvents = true;
            this.colorsObjectListView.UseCompatibleStateImageBehavior = false;
            this.colorsObjectListView.UseFiltering = true;
            this.colorsObjectListView.UseSubItemCheckBoxes = true;
            this.colorsObjectListView.View = System.Windows.Forms.View.Details;
            this.colorsObjectListView.CellEditStarting += new BrightIdeasSoftware.CellEditEventHandler(this.colorsObjectListView_CellEditStarting);
            this.colorsObjectListView.CellEditValidating += new BrightIdeasSoftware.CellEditEventHandler(this.colorsObjectListView_CellEditValidating);
            this.colorsObjectListView.SubItemChecking += new System.EventHandler<BrightIdeasSoftware.SubItemCheckingEventArgs>(this.colorsObjectListView_SubItemChecking);
            this.colorsObjectListView.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.colorsObjectListView_FormatCell);
            this.colorsObjectListView.SelectionChanged += new System.EventHandler(this.colorsObjectListView_SelectionChanged);
            this.colorsObjectListView.DoubleClick += new System.EventHandler(this.colorsObjectListView_DoubleClick);
            // 
            // colorTextColumn
            // 
            this.colorTextColumn.AspectName = "Text";
            this.colorTextColumn.CellPadding = null;
            this.colorTextColumn.IsEditable = false;
            this.colorTextColumn.Text = "Name";
            this.colorTextColumn.UseFiltering = false;
            this.colorTextColumn.Width = 140;
            // 
            // colorColorColumn
            // 
            this.colorColorColumn.AspectName = "Color";
            this.colorColorColumn.AspectToStringFormat = "";
            this.colorColorColumn.CellPadding = null;
            this.colorColorColumn.Sortable = false;
            this.colorColorColumn.Text = "Color";
            this.colorColorColumn.UseFiltering = false;
            this.colorColorColumn.Width = 80;
            // 
            // colorValueColumn
            // 
            this.colorValueColumn.AspectName = "ColorString";
            this.colorValueColumn.CellPadding = null;
            this.colorValueColumn.Sortable = false;
            this.colorValueColumn.Text = "Color Value";
            this.colorValueColumn.UseFiltering = false;
            this.colorValueColumn.Width = 75;
            // 
            // colorIntensityColumn
            // 
            this.colorIntensityColumn.AspectName = "Scales";
            this.colorIntensityColumn.CellPadding = null;
            this.colorIntensityColumn.CheckBoxes = true;
            this.colorIntensityColumn.FillsFreeSpace = true;
            this.colorIntensityColumn.MinimumWidth = 16;
            this.colorIntensityColumn.Sortable = false;
            this.colorIntensityColumn.Text = "Intensity?";
            this.colorIntensityColumn.UseFiltering = false;
            // 
            // colorsContextMenuStrip
            // 
            this.colorsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editSelectedToolStripMenuItem,
            this.resetSelectedToolStripMenuItem});
            this.colorsContextMenuStrip.Name = "colorsContextMenuStrip";
            this.colorsContextMenuStrip.Size = new System.Drawing.Size(146, 48);
            this.colorsContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.colorsContextMenuStrip_Opening);
            // 
            // editSelectedToolStripMenuItem
            // 
            this.editSelectedToolStripMenuItem.Image = global::HammerThemes.Properties.Resources.pencil;
            this.editSelectedToolStripMenuItem.Name = "editSelectedToolStripMenuItem";
            this.editSelectedToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.editSelectedToolStripMenuItem.Text = "Edit selected";
            this.editSelectedToolStripMenuItem.Click += new System.EventHandler(this.editSelectedToolStripMenuItem_Click);
            // 
            // resetSelectedToolStripMenuItem
            // 
            this.resetSelectedToolStripMenuItem.Image = global::HammerThemes.Properties.Resources.arrow_undo;
            this.resetSelectedToolStripMenuItem.Name = "resetSelectedToolStripMenuItem";
            this.resetSelectedToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.resetSelectedToolStripMenuItem.Text = "Reset selected";
            this.resetSelectedToolStripMenuItem.Click += new System.EventHandler(this.resetSelectedToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.previewPanel);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(493, 514);
            this.panel1.TabIndex = 9;
            // 
            // previewPanel
            // 
            this.previewPanel.ChangeCursor = true;
            this.previewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewPanel.DottedGrid = false;
            this.previewPanel.Grid1024Distance = 80;
            this.previewPanel.GridIntensity = ((byte)(255));
            this.previewPanel.IsAboutScreen = false;
            this.previewPanel.Location = new System.Drawing.Point(0, 26);
            this.previewPanel.Name = "previewPanel";
            this.previewPanel.Size = new System.Drawing.Size(493, 488);
            this.previewPanel.TabIndex = 7;
            this.previewPanel.Theme = null;
            this.previewPanel.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.dottedGridCheckBox);
            this.flowLayoutPanel1.Controls.Add(this.label4);
            this.flowLayoutPanel1.Controls.Add(this.gridSizeTrackBar);
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Controls.Add(this.gridIntensityTrackBar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(493, 26);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // dottedGridCheckBox
            // 
            this.dottedGridCheckBox.AutoSize = true;
            this.dottedGridCheckBox.Checked = global::HammerThemes.Properties.Settings.Default.PreviewGridDotted;
            this.dottedGridCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::HammerThemes.Properties.Settings.Default, "PreviewGridDotted", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dottedGridCheckBox.Location = new System.Drawing.Point(3, 3);
            this.dottedGridCheckBox.Name = "dottedGridCheckBox";
            this.dottedGridCheckBox.Size = new System.Drawing.Size(78, 17);
            this.dottedGridCheckBox.TabIndex = 6;
            this.dottedGridCheckBox.Text = "Dotted grid";
            this.dottedGridCheckBox.UseVisualStyleBackColor = true;
            this.dottedGridCheckBox.CheckedChanged += new System.EventHandler(this.dottedGridCheckBox_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 4);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 4, 0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Grid size:";
            // 
            // gridSizeTrackBar
            // 
            this.gridSizeTrackBar.AutoSize = false;
            this.gridSizeTrackBar.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::HammerThemes.Properties.Settings.Default, "PreviewGridSize", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gridSizeTrackBar.Location = new System.Drawing.Point(137, 3);
            this.gridSizeTrackBar.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.gridSizeTrackBar.Maximum = 64;
            this.gridSizeTrackBar.Name = "gridSizeTrackBar";
            this.gridSizeTrackBar.Size = new System.Drawing.Size(108, 20);
            this.gridSizeTrackBar.TabIndex = 3;
            this.gridSizeTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.gridSizeTrackBar.Value = global::HammerThemes.Properties.Settings.Default.PreviewGridSize;
            this.gridSizeTrackBar.Scroll += new System.EventHandler(this.gridSizeTrackBar_Scroll);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(251, 4);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 4, 0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Grid intensity:";
            // 
            // gridIntensityTrackBar
            // 
            this.gridIntensityTrackBar.AutoSize = false;
            this.gridIntensityTrackBar.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::HammerThemes.Properties.Settings.Default, "PreviewGridIntensity", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gridIntensityTrackBar.Location = new System.Drawing.Point(321, 3);
            this.gridIntensityTrackBar.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.gridIntensityTrackBar.Maximum = 255;
            this.gridIntensityTrackBar.Name = "gridIntensityTrackBar";
            this.gridIntensityTrackBar.Size = new System.Drawing.Size(108, 20);
            this.gridIntensityTrackBar.TabIndex = 5;
            this.gridIntensityTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.gridIntensityTrackBar.Value = global::HammerThemes.Properties.Settings.Default.PreviewGridIntensity;
            this.gridIntensityTrackBar.Scroll += new System.EventHandler(this.gridIntensityTrackBar_Scroll);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(493, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Preview:";
            // 
            // settingsButton
            // 
            this.settingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.settingsButton.Image = global::HammerThemes.Properties.Resources.cog;
            this.settingsButton.Location = new System.Drawing.Point(712, 2);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(80, 29);
            this.settingsButton.TabIndex = 11;
            this.settingsButton.Text = "&Settings";
            this.settingsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.settingsButton.UseVisualStyleBackColor = true;
            this.settingsButton.Click += new System.EventHandler(this.settingsButton_Click);
            // 
            // Main
            // 
            this.AcceptButton = this.applyButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 591);
            this.Controls.Add(this.footerSplitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(532, 349);
            this.Name = "Main";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Text = "HammerThemes";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.Main_HelpRequested);
            this.footerSplitContainer.Panel1.ResumeLayout(false);
            this.footerSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.footerSplitContainer)).EndInit();
            this.footerSplitContainer.ResumeLayout(false);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel1.PerformLayout();
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorsObjectListView)).EndInit();
            this.colorsContextMenuStrip.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSizeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridIntensityTrackBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox;
        private BrightIdeasSoftware.ObjectListView colorsObjectListView;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox themesComboBox;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button cloneButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Label themeLabel;
        private BrightIdeasSoftware.OLVColumn colorTextColumn;
        private BrightIdeasSoftware.OLVColumn colorNameColumn;
        private BrightIdeasSoftware.OLVColumn colorColorColumn;
        private BrightIdeasSoftware.OLVColumn colorValueColumn;
        private BrightIdeasSoftware.OLVColumn colorDescriptionColumn;
        private System.Windows.Forms.Label helpLabel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.CheckBox unusedCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.Button donateButton;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.CheckBox dottedGridCheckBox;
        private System.Windows.Forms.TrackBar gridIntensityTrackBar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar gridSizeTrackBar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private PreviewPanel previewPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.SplitContainer footerSplitContainer;
        private BrightIdeasSoftware.OLVColumn colorIntensityColumn;
        private System.Windows.Forms.ContextMenuStrip colorsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetSelectedToolStripMenuItem;
        private System.Windows.Forms.Button settingsButton;
        private System.Windows.Forms.Button aboutButton;
    }
}

