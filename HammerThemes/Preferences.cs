﻿using HammerThemes.Properties;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// Settings dialog for HammerThemes.
    /// </summary>
    public partial class Preferences : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Preferences"/> class.
        /// </summary>
        public Preferences()
        {
            InitializeComponent();

            //Data-bindings
            this.updateFrequencyComboBox.DataBindings.Add("SelectedIndex", Settings.Default, "UpdateFrequency", false, DataSourceUpdateMode.OnPropertyChanged);
            this.updateAvailableRadioGroupPanel.DataBindings.Add("Selected", Settings.Default, "UpdateAvailable", false, DataSourceUpdateMode.OnPropertyChanged);
            Settings.Default.Save();
        }

        private async void updateCheckButton_Click(object sender, System.EventArgs e)
        {
            //Start update check
            this.checkingLabel.Show();
            this.updateCheckButton.Enabled = false;
            var success = await Updates.CheckUpdateAsync();
            this.checkingLabel.Hide();
            if (success)
            {
                //Is there a new version?
                if (Updates.IsOutdated)
                {
                    //OMG INFORM ME NOW
                    Updates.ShowUpdateAvailable(true);
                }
                else
                {
                    //Latest version
                    Updates.ShowUpToDate();
                }
            }
            else
            {
                //Error!
                Updates.ShowUpdateError();
            }
            this.updateCheckButton.Enabled = true;
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            this._saveSettings = true;
            this.Close();
        }

        private bool _saveSettings;

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void Preferences_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this._saveSettings)
            {
                Settings.Default.Save();
            }
            else
            {
                Settings.Default.Reload();
            }
        }

        private void Preferences_Load(object sender, System.EventArgs e)
        {
            this.CenterToParent();
        }
    }
}
