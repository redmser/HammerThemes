﻿namespace HammerThemes
{
    partial class ColorEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.resetButton = new System.Windows.Forms.Button();
            this.pickButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // colorDialog
            // 
            this.colorDialog.AnyColor = true;
            this.colorDialog.FullOpen = true;
            // 
            // resetButton
            // 
            this.resetButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetButton.Image = global::HammerThemes.Properties.Resources.arrow_undo;
            this.resetButton.Location = new System.Drawing.Point(129, 0);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(20, 23);
            this.resetButton.TabIndex = 1;
            this.toolTip.SetToolTip(this.resetButton, "Reset the color to Hammer\'s default value");
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // pickButton
            // 
            this.pickButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pickButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pickButton.Image = global::HammerThemes.Properties.Resources.pencil;
            this.pickButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.pickButton.Location = new System.Drawing.Point(0, 0);
            this.pickButton.Margin = new System.Windows.Forms.Padding(0);
            this.pickButton.Name = "pickButton";
            this.pickButton.Size = new System.Drawing.Size(129, 23);
            this.pickButton.TabIndex = 0;
            this.toolTip.SetToolTip(this.pickButton, "Click to edit the color");
            this.pickButton.UseVisualStyleBackColor = true;
            this.pickButton.Click += new System.EventHandler(this.pickButton_Click);
            // 
            // ColorEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.pickButton);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ColorEditor";
            this.Size = new System.Drawing.Size(151, 23);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button pickButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.ToolTip toolTip;
        internal System.Windows.Forms.ColorDialog colorDialog;
    }
}
