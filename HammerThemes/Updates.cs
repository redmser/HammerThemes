﻿using System.Threading.Tasks;
using GitlabVersioning;
using System.Security;
using System.Reflection;
using System.IO;
using System;
using System.Diagnostics;
using System.Windows.Forms;
using HammerThemes.Properties;
using System.Threading;
using System.Net;

namespace HammerThemes
{
    /// <summary>
    /// Helper class for updating.
    /// </summary>
    public static class Updates
    {
        //This class handles all of HammerThemes' updating logic. The library "GitLabVersioning" is responsible for retrieving the required information.
        //If you wish to fork HammerThemes and keep updating in tact, be sure to:
        //  - Update the constants below (Project ID can be gotten from the General Project Preferences, URL should be formatted as below)

        internal const int ProjectID = 4463058;
        internal const string ProjectURL = @"https://gitlab.com/redmser/HammerThemes";

        /// <summary>
        /// Gets the known updates.
        /// </summary>
        public static TagList Tags { get; private set; }

        /// <summary>
        /// Gets whether this version is outdated.
        /// </summary>
        public static bool IsOutdated
        {
            get
            {
                var vOnline = new Version(LatestVersion);
                var vLocal = new Version(CurrentVersion);
                return vOnline > vLocal;
            }
        }

        static Updates()
        {
            _updateWebClient = new WebClient();
            _updateWebClient.Proxy = null;
        }

        private readonly static WebClient _updateWebClient;

        /// <summary>
        /// Shows the latest update error that occurred.
        /// </summary>
        public static void ShowUpdateError()
            => MessageBox.Show("An error occurred trying to check for updates:\n\n" + Updates.Tags.LastError.Message, "Error updating", MessageBoxButtons.OK, MessageBoxIcon.Error);

        /// <summary>
        /// Shows the dialog for being up to date.
        /// </summary>
        public static void ShowUpToDate()
            => MessageBox.Show($"You already have the latest version (v{Updates.CurrentVersion}) installed.", "Up-to-date", MessageBoxButtons.OK, MessageBoxIcon.Information);

        /// <summary>
        /// Shows the dialog for a new update being available. Returns which option was selected.
        /// </summary>
        /// <param name="manualUpdate">Whether this update was manually triggered.</param>
        /// <param name="path">If specified, the update file has already been downloaded to this path and will not be re-downloaded.</param>
        /// <returns></returns>
        public static DialogResult ShowUpdateAvailable(bool manualUpdate, string path = null)
        {
            //Is skipped?
            if (!manualUpdate && Settings.Default.UpdateSkipVersion == Updates.LatestVersion)
                return DialogResult.Ignore;

            return new UpdateAvailable(manualUpdate, path).ShowDialog();
        }

        /// <summary>
        /// Shows the dialog for the download being done.
        /// </summary>
        /// <param name="justApplyIt">Contrary to the name of the method, setting this to <c>true</c> will simply install the update.</param>
        /// <param name="path">Path to the downloaded release file.</param>
        public static void ShowDownloadDone(bool justApplyIt, string path)
        {
            //NYI: Updates - install HammerThemes zip using a secondary executable which extracts, replaces and re-opens
            DialogResult res;
            if (justApplyIt)
                res = DialogResult.Yes;
            else
                res = MessageBox.Show("The update has been downloaded successfully and can be found as a zip file in HammerThemes' installation folder.\n" +
                    "Would you like to install the update now? (This will close HammerThemes!)",
                "Install update now?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (res == DialogResult.Yes)
            {
                //Open zip, close program
                Process.Start(path);
                Application.Exit();
            }
            else
            {
                //Skip this version
                Settings.Default.UpdateSkipVersion = Updates.LatestVersion;
            }
        }

        /// <summary>
        /// Gets the installed version of the program.
        /// </summary>
        public static string CurrentVersion => FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;

        /// <summary>
        /// Gets the latest version of the program, as retrieved from a previous <see cref="CheckUpdateAsync"/> call.
        /// </summary>
        public static string LatestVersion => Tags[0].TagName.Substring(1);

        /// <summary>
        /// Downloads the release of the latest version, as retrieved from a previous <see cref="CheckUpdateAsync"/> call.
        /// </summary>
        /// <param name="cancel">Cancellation token.</param>
        /// <param name="progress">Progress reporter (0% to 100%).</param>
        /// <returns></returns>
        public static async Task<string> DownloadUpdateAsync(CancellationToken cancel, IProgress<int> progress)
        {
            //Init
            var release = Updates.Tags[0].Releases[0];
            var path = Path.Combine(Application.StartupPath, release.Name + ".zip");

            //Allow cancel
            cancel.Register(() => _updateWebClient.CancelAsync());

            //Report progress
            _updateWebClient.DownloadProgressChanged += (s, e) => progress.Report(e.ProgressPercentage);

            await _updateWebClient.DownloadFileTaskAsync(release.Url, path);

            if (cancel.IsCancellationRequested)
                return null;

            return path;
        }

        /// <summary>
        /// Checks for updates without blocking.
        /// </summary>
        /// <returns></returns>
        public static Task<bool> CheckUpdateAsync()
        {
            if (Tags == null)
            {
                Tags = new TagList(ProjectID);
                Tags.PreloadAbsoluteUrl = false;
                Tags.ProjectUrl = ProjectURL;
            }

            return Task.Run(() => Tags.LoadTags());
        }
    }
}
