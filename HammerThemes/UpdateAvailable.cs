﻿using HammerThemes.Properties;
using System;
using System.Threading;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// The dialog to display when a new version of the software is available.
    /// </summary>
    /// <remarks>The DialogResult "Ignore" is used for the "Skip this version" option.</remarks>
    public partial class UpdateAvailable : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateAvailable"/> class.
        /// </summary>
        public UpdateAvailable(bool manualUpdate, string path)
        {
            //Compiler-generated
            InitializeComponent();

            this.Path = path;
            this.disableAutoUpdatesCheckBox.Visible = !manualUpdate;
            this.remindButton.MenuCancelsClick = true;
            this.remindButton.SplitButton = true;

            //Update version info
            this.versionLabel.Text = string.Format(this.versionLabel.Text, Updates.LatestVersion, Updates.CurrentVersion);

            //Populate changes list
            //TODO: UpdateChangesList - turn top-level bullet points ("categories") into OLV Groups, reduce indent of sub-items and have those as entries of the groups
            this.changesObjectListView.SetObjects(Updates.Tags[0].Changes);
        }

        internal UpdateAvailable() : this(false, null) { }

        /// <summary>
        /// Gets or sets the path to the downloaded .zip update file, if the update is already on disk.
        /// </summary>
        public string Path { get; set; }
        
        /// <summary>
        /// Gets the amount of days that the "Remind me later action" should delay this update notification for.
        /// </summary>
        public int RemindLaterDays { get; private set; }

        private void remindMeIn1DayToolStripMenuItem_Click(object sender, EventArgs e) => RemindMeLater(1);

        private void remindMeIn1WeekToolStripMenuItem_Click(object sender, EventArgs e) => RemindMeLater(7);

        private void remindMeIn1MonthToolStripMenuItem_Click(object sender, EventArgs e) => RemindMeLater(30);

        private void RemindMeLater(int days)
        {
            this.RemindLaterDays = days;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private CancellationTokenSource _cancelDownload;

        private async void UpdateAvailable_FormClosing(object sender, FormClosingEventArgs e)
        {
            //UNTESTED: UpdateAvailable - cancellation of in-progress download

            //Save checkbox state
            if (this.disableAutoUpdatesCheckBox.Visible && this.disableAutoUpdatesCheckBox.Checked)
                Settings.Default.UpdateFrequency = 0;

            //Is downloading?
            if (this._cancelDownload != null)
            {
                //Ask whether to cancel download
                var res = MessageBox.Show("Would you like to stop updating HammerThemes?", "Cancel update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
                else
                {
                    //Cancel the download, bois
                    this._cancelDownload.Cancel();

                    //Count as a "skip version"
                    Settings.Default.UpdateSkipVersion = Updates.LatestVersion;
                    return;
                }
            }

            switch (this.DialogResult)
            {
                case DialogResult.OK:
                    //Is already downloaded?
                    if (this.Path != null)
                    {
                        Updates.ShowDownloadDone(true, this.Path);
                        return;
                    }

                    //Download update
                    e.Cancel = true;
                    this.downloadingProgressPanel.Show();
                    this.remindButton.Enabled = false;
                    this.skipButton.Enabled = false;
                    this.updateButton.Enabled = false;
                    var progress = new Progress<int>();
                    progress.ProgressChanged += (s, o) => this.TryInvoke(() => this.updateProgressBar.Value = o);
                    this._cancelDownload = new CancellationTokenSource();

                    var path = await Updates.DownloadUpdateAsync(this._cancelDownload.Token, progress);

                    //Download is done, let user know!
                    e.Cancel = false;
                    this._cancelDownload = null;
                    this.DialogResult = DialogResult.Abort;
                    Updates.ShowDownloadDone(false, path);
                    break;
                case DialogResult.Ignore:
                    //Skip this version, don't remind me about it anymore
                    Settings.Default.UpdateSkipVersion = Updates.LatestVersion;
                    break;
                case DialogResult.Cancel:
                    //Remind me later
                    Settings.Default.UpdateRemindDate = DateTime.Now.AddDays(this.RemindLaterDays);
                    break;
            }
        }

        private void UpdateAvailable_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }
    }
}
