﻿using System.Diagnostics;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// Dialog to show infos and help for HammerThemes.
    /// </summary>
    public partial class About : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="About"/> class.
        /// </summary>
        public About() : this(null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="About"/> class. Includes infos for generating the logo.
        /// </summary>
        /// <param name="original">The <see cref="PreviewPanel"/> to generate the logo from.</param>
        public About(PreviewPanel original)
        {
            InitializeComponent();

            //Init preview panel
            //FIXME: About - live-update variable changes too
            this.previewPanel.Theme = original.Theme;
            this.previewPanel.PreviewBrush = new GoodRectangle();
            this.previewPanel.DottedGrid = original?.DottedGrid ?? false;
            this.previewPanel.GridIntensity = original?.GridIntensity ?? 255;
            this.previewPanel.Show();

            //Update info label
            this.infoLabel.Text = $"Version {Updates.CurrentVersion}\nBy RedMser";
        }
        
        private void gitlabButton_Click(object sender, System.EventArgs e)
        {
            //Open gitlab page
            Process.Start(Updates.ProjectURL);
        }

        private void About_Load(object sender, System.EventArgs e)
        {
            this.CenterToParent();
        }
    }
}
