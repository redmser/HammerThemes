﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// A panel which automatically updates itself to resemble the preview for a Hammer Editor 2d viewport.
    /// </summary>
    public class PreviewPanel : Panel
    {
        //BUG: PreviewPanel - when moved off-screen, does not correctly update when dragged back (until fully invalidated)
        //BUG: PreviewPanel - resizing the panel does not cause the PreviewBrush to move accordingly

        /// <summary>
        /// Initializes a new instance of the <see cref="PreviewPanel"/> class.
        /// </summary>
        public PreviewPanel()
        {
            this.ResizeRedraw = true;
            this.DoubleBuffered = true;
        }

        /// <summary>
        /// Gets or sets the color theme.
        /// </summary>
        public Theme Theme { get; set; }

        /// <summary>
        /// Gets or sets the size of the grid, in pixels.
        /// </summary>
        public int GridSize { get; set; } = 12;

        /// <summary>
        /// Gets or sets the alpha value of the grid.
        /// </summary>
        public byte GridIntensity { get; set; } = 255;

        /// <summary>
        /// Gets or sets the distance, in pixels, for the 1024 grid highlight lines.
        /// </summary>
        public int Grid1024Distance { get; set; } = 80;

        /// <summary>
        /// Gets or sets whether to display a dotted grid.
        /// </summary>
        public bool DottedGrid { get; set; }

        /// <summary>
        /// Gets or sets whether this preview panel is used on the About screen. Changes handling and rendering logic.
        /// </summary>
        public bool IsAboutScreen { get; set; }

        /// <summary>
        /// Gets or sets the bounds of the preview brush that can be manipulated.
        /// </summary>
        [ReadOnly(true)]
        [Browsable(false)]
        public GoodRectangle PreviewBrush { get; set; }

        /// <summary>
        /// Gets or sets whether the panel may change the cursor.
        /// </summary>
        public bool ChangeCursor { get; set; } = true;

        /// <summary>
        /// Whether the user is dragging the preview brush.
        /// </summary>
        protected bool _dragging = false;

        /// <summary>
        /// The mouse offset when dragging.
        /// </summary>
        protected Point _mouseOffset;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            //Check if mouse is over brush
            if (this.PreviewBrush.Contains(e.Location))
            {
                this._mouseOffset = new Point(e.X - this.PreviewBrush.Left, e.Y - this.PreviewBrush.Top);
                this._dragging = true;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            this._dragging = false;
            this.Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            //NYI: PreviewPanel - allow resizing the PreviewBrush using the handles
            //NYI: PreviewPanel - if dragging outside of the PreviewBrush, create a new PreviewBrush instead (only with block tool mode!)
            //  (in selection mode, obviously create a box selection and allow for deselecting the brush as well)
            //  -> may help to put this logic into another class, and simply calling a Paint function for the brush instance instead
            if (this.ChangeCursor)
            {
                //Change cursor depending on state
                if (this.PreviewBrush.Contains(e.Location) || this._dragging)
                {
                    //Move brush
                    this.Cursor = Cursors.SizeAll;
                }
                else
                {
                    //Reset cursor
                    this.Cursor = this.DefaultCursor;
                }
            }

            //Is dragging?
            if (this._dragging)
            {
                //Update cross position
                this._crossPos = ClosestCorner(this.PreviewBrush, e.Location, false);

                //Move brush location
                this.PreviewBrush = new GoodRectangle(e.X - this._mouseOffset.X, e.Y - this._mouseOffset.Y, this.PreviewBrush.Width, this.PreviewBrush.Height);
                this.ClampPreviewBrush();
                this.Invalidate();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ClientSizeChanged" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnClientSizeChanged(EventArgs e)
        {
            this.ClampPreviewBrush();
            base.OnClientSizeChanged(e);
        }

        /// <summary>
        /// Ensures that the preview brush does not leave the window bounds.
        /// </summary>
        protected void ClampPreviewBrush() => this.PreviewBrush = this.PreviewBrush.Clamp(new Rectangle(Point.Empty, this.Size));

        /// <summary>
        /// Location of the movement cross.
        /// </summary>
        protected Point _crossPos;

        /// <summary>
        /// Gets the corner position of the rectangle that the specified location is closest to.
        /// </summary>
        /// <param name="rect">The rectangle.</param>
        /// <param name="loc">The location.</param>
        /// <param name="locIsRelative">If set to <c>true</c>, location is relative - 0;0 is topleft of rectangle.</param>
        /// <returns></returns>
        protected Point ClosestCorner(GoodRectangle rect, Point loc, bool locIsRelative)
        {
            if (locIsRelative)
                loc = new Point(loc.X + rect.Left, loc.Y + rect.Top);
            if (loc.X > rect.CenterX)
            {
                if (loc.Y > rect.CenterY)
                    return rect.BottomRight;
                return rect.TopRight;
            }
            else
            {
                if (loc.Y > rect.CenterY)
                    return rect.BottomLeft;
                return rect.TopLeft;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            if (this.DesignMode || !this.Visible)
            {
                e.Graphics.Clear(Color.Red);
                return;
            }

            //Get selected theme and its colors
            var theme = this.Theme ?? Properties.Settings.Default.Themes.OfType<DefaultTheme>().FirstOrDefault();
            if (theme == null)
                return;

            var rect = new GoodRectangle(e.ClipRectangle);

            //Background
            e.Graphics.Clear(theme.GetColor("Background").Color);

            //Grid
            var dist = this.GridSize;
            var intensity = this.GridIntensity;
            if (dist != 0)
            {
                Pen gridpen;
                Pen grid10pen;

                if (this.DottedGrid)
                {
                    //Dotted grid - don't show normal lines
                    gridpen = new Pen(Color.Empty);
                    grid10pen = new Pen(Color.Empty);

                    var griddotbrush = new SolidBrush(theme.GetColor("GridDot").ApplyAlpha(intensity));

                    for (var x = rect.HalfWidth % dist; x < rect.Width; x += dist)
                        for (var y = rect.HalfHeight % dist; y < rect.Height; y += dist)
                            e.Graphics.FillRectangle(griddotbrush, rect.Left + x, rect.Top + y, 1, 1);
                }
                else
                {
                    //Initialize grid/grid10 pens
                    gridpen = new Pen(theme.GetColor("Grid").ApplyAlpha(intensity));
                    grid10pen = new Pen(theme.GetColor("Grid10").ApplyAlpha(intensity));
                }

                //Grid lines (always draw 1024ers)
                var grid1024pen = new Pen(theme.GetColor("Grid1024").ApplyAlpha(intensity));

                //Horizontal
                var carrydist = rect.HalfHeight % dist;
                var highdist = rect.HalfHeight % dist;
                for (var y = rect.HalfHeight % dist; y < rect.Height; y += dist)
                {
                    var pen = gridpen;
                    if (this.IsAboutScreen && TriggerIconLines(carrydist))
                    {
                        //Special handling code
                        pen = grid1024pen;
                    }
                    else
                    {
                        if (carrydist >= this.Grid1024Distance)
                        {
                            carrydist = 0;
                            pen = grid1024pen;
                        }
                        else if (highdist >= dist * 4)
                        {
                            highdist = 0;
                            pen = grid10pen;
                        }
                    }

                    e.Graphics.DrawLine(pen, new Point(rect.Left, rect.Top + y), new Point(rect.Right, rect.Top + y));
                    carrydist += dist;
                    highdist += dist;
                }

                //Vertical
                carrydist = rect.HalfWidth % dist;
                highdist = rect.HalfWidth % dist;
                for (var x = rect.HalfWidth % dist; x < rect.Width; x += dist)
                {
                    var pen = gridpen;
                    if (this.IsAboutScreen && TriggerIconLines(carrydist))
                    {
                        //Special handling code
                        pen = grid1024pen;
                    }
                    else
                    {
                        if (carrydist >= this.Grid1024Distance)
                        {
                            carrydist = 0;
                            pen = grid1024pen;
                        }
                        else if (highdist >= dist * 4)
                        {
                            highdist = 0;
                            pen = grid10pen;
                        }
                    }

                    e.Graphics.DrawLine(pen, new Point(rect.Left + x, rect.Top), new Point(rect.Left + x, rect.Bottom));
                    carrydist += dist;
                    highdist += dist;
                }
            }

            //Origin at center
            var grid0pen = new Pen(theme.GetColor("Grid0").ApplyAlpha(intensity));
            e.Graphics.DrawLine(grid0pen, rect.LeftCenter, rect.RightCenter);
            e.Graphics.DrawLine(grid0pen, rect.TopCenter, rect.BottomCenter);

            if (this.PreviewBrush.Width > 0 && this.PreviewBrush.Height > 0)
            {
                //NYI: PreviewPanel - add a way to toggle between block tool preview and brush selection preview (different colors used, cross in center, and vertices visible)

                //Block tool preview
                Pen blockpen;
                if (this._dragging)
                {
                    //Use dragging pen
                    blockpen = new Pen(theme.GetColor("ToolDrag").Color);

                    //Add cross at corner that mouse is closest to
                    e.Graphics.DrawLine(blockpen, this._crossPos.X - 6, this._crossPos.Y - 6, this._crossPos.X + 6, this._crossPos.Y + 6);
                    e.Graphics.DrawLine(blockpen, this._crossPos.X - 6, this._crossPos.Y + 6, this._crossPos.X + 6, this._crossPos.Y - 6);
                }
                else
                {
                    //Use block tool pen
                    blockpen = new Pen(theme.GetColor("BoxColor").Color);

                    //Only show handles if not dragging
                    ResizeHandles(this.PreviewBrush);
                }
                blockpen.DashStyle = DashStyle.Custom;
                blockpen.DashPattern = new float[] { 8, 8 };
                e.Graphics.DrawRectangle(blockpen, this.PreviewBrush);

                //Add size text
                e.Graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixelGridFit;
                var sizefont = new Font("Tahoma", 9);
                var widthtext = this.PreviewBrush.Width + ".0";
                var widthtextsize = e.Graphics.MeasureString(widthtext, sizefont);
                var heighttext = this.PreviewBrush.Height + ".0";
                var heighttextsize = e.Graphics.MeasureString(heighttext, sizefont);
                e.Graphics.DrawString(widthtext, sizefont, Brushes.White,
                    this.PreviewBrush.CenterX - widthtextsize.Width / 2, this.PreviewBrush.Top - 11 - widthtextsize.Height);
                e.Graphics.DrawString(heighttext, sizefont, Brushes.White,
                    this.PreviewBrush.Left - heighttextsize.Width - 11, this.PreviewBrush.CenterY - heighttextsize.Height / 2);
            }



            bool TriggerIconLines(int carrydist)
            {
                if (carrydist == 10 || carrydist == 24)
                    return true;
                return false;
            }

            void ResizeHandles(GoodRectangle handlerect)
            {
                var handlebrush = new SolidBrush(theme.GetColor("HandleColor").Color);
                ResizeHandle(new GoodRectangle(handlerect.Left - 11, handlerect.Top - 11, 9, 9));
                ResizeHandle(new GoodRectangle(handlerect.CenterX - 4, handlerect.Top - 11, 9, 9));
                ResizeHandle(new GoodRectangle(handlerect.Right + 2, handlerect.Top - 11, 9, 9));

                ResizeHandle(new GoodRectangle(handlerect.Left - 11, handlerect.CenterY - 4, 9, 9));
                ResizeHandle(new GoodRectangle(handlerect.Right + 2, handlerect.CenterY - 4, 9, 9));

                ResizeHandle(new GoodRectangle(handlerect.Left - 11, handlerect.Bottom + 2, 9, 9));
                ResizeHandle(new GoodRectangle(handlerect.CenterX - 4, handlerect.Bottom + 2, 9, 9));
                ResizeHandle(new GoodRectangle(handlerect.Right + 2, handlerect.Bottom + 2, 9, 9));



                void ResizeHandle(GoodRectangle handle)
                {
                    e.Graphics.FillRectangle(handlebrush, handle);
                    e.Graphics.DrawRectangle(Pens.Black, handle);
                }
            }
        }
    }
}
