﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace HammerThemes
{
    /// <summary>
    /// Color editing control for the ObjectListView.
    /// </summary>
    public partial class ColorEditor : UserControl
    {
        //TODO: ColorEditor - add a "pick color from screen" option of sorts

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorEditor"/> class.
        /// </summary>
        public ColorEditor()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorEditor"/> class.
        /// </summary>
        /// <param name="model">The model object used in display.</param>
        public ColorEditor(object model) : this()
        {
            if (model is ThemeColor tc)
            {
                this.Color = tc;
            }
        }

        /// <summary>
        /// Gets the themecolor to get the default value from.
        /// </summary>
        public ThemeColor Color { get; }

        private void pickButton_Click(object sender, System.EventArgs e)
        {
            this.colorDialog.Color = this.pickButton.BackColor;
            if (this.colorDialog.ShowDialog() == DialogResult.OK)
            {
                //Apply color
                this.pickButton.BackColor = this.colorDialog.Color;
            }
        }

        /// <summary>
        /// Gets or sets the color value of the editor.
        /// </summary>
        public Color Value
        {
            get => this.pickButton.BackColor;
            set => this.pickButton.BackColor = value;
        }

        private void resetButton_Click(object sender, System.EventArgs e)
        {
            //Reset to default
            //FIXME: ColorEditor - in any case where a themecolor is "Disabled", it should automatically fall back to the default - no need to manually specify the new color
            this.Value = Theme.DefaultColors.FirstOrDefault(t => t.Name == this.Color.Name).Color;
            this.Color.Enabled = false;
        }
    }
}
